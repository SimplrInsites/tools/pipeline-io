//! Number utilities
//!
//! ---
//! author: Andrew Evans
//! ---


pub fn get_max_i64(n_vec: Vec<(usize, i64)>) -> (usize, i64){
    let mut mx = 0;
    let mut mx_idx = 0;
    for (idx, n) in n_vec{
        if n > mx  || idx == 0{
            mx = n;
            mx_idx = idx;
        }
    }
    (mx_idx, mx)
}


#[cfg(test)]
pub mod tests{
    use crate::number::utils::get_max_i64;

    #[test]
    fn should_get_max_u32(){
        let mut v = Vec::<(usize, i64)>::new();
        v.push((0, 10));
        v.push((1, 14));
        v.push((2, 9));
        let (idx, n) = get_max_i64(v);
        assert_eq!(idx, 1);
        assert_eq!(n, 14);
    }
}
