use chrono::NaiveDateTime;

/// Get the max date index
///
/// # Variables
/// * `date_vec` - Date vector with index and date time tuples
pub fn get_max_date(date_vec: Vec<(usize, NaiveDateTime)>) -> (usize, NaiveDateTime){
    let mut max_dt = NaiveDateTime::parse_from_str(
        "1970-01-01 00:00:00", "%Y-%m-%d %H:%M:%S").unwrap();
    let mut max_idx = 0;
    for (idx, dt) in date_vec{
        if idx == 0{
            max_dt = dt.clone();
            max_idx = idx.clone();
        }else{
            let dur = max_dt.signed_duration_since(dt);
            if dur.num_days() < 0{
                max_dt = dt.clone();
                max_idx = idx.clone();
            }
        }
    }
    (max_idx, max_dt)
}


#[cfg(test)]
pub mod tests{
    use chrono::Duration;
    use std::ops::Sub;
    use crate::time::chrono_utils::get_max_date;

    #[test]
    fn should_find_the_max_timestamp(){
        let dt1 = chrono::Utc::now().sub(Duration::days(120));
        let dt2 = chrono::Utc::now();
        let dt3 = chrono::Utc::now().sub(Duration::days(50));
        let ndt1 = dt1.naive_utc();
        let ndt2 = dt2.naive_utc();
        let ndt3 = dt3.naive_utc();
        let ndays = ndt1.signed_duration_since(ndt2).num_days();
        let mut v = Vec::new();
        assert!(ndays < 0);
        v.push((0, ndt3));
        v.push((1, ndt2.clone()));
        v.push((2, ndt1));
        let (idx, dt) = get_max_date(v);
        assert_eq!(idx, 1);
        let days_since2 = ndt2.signed_duration_since(dt).num_days();
        assert_eq!(days_since2, 0 );
    }
}
