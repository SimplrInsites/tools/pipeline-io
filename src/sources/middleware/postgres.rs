//! Used to insert records into Postgres
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;

use serde_json::Value;
use tokio_postgres::Row;

#[derive(Clone)]
pub struct PostgreConverter{
    pub f: fn(Row) -> Result<HashMap<String, Value>, String>
}

#[cfg(test)]
pub mod tests{
    use tokio_postgres::Row;
    use std::collections::HashMap;
    use serde_json::Value;
    use crate::sources::middleware::postgres::PostgreConverter;

    /// Test function
    fn test_fn(_r: Row) -> Result<HashMap<String, Value>, String>{
        Ok(HashMap::new())
    }

    #[test]
    fn should_clone(){
        let converter = PostgreConverter{
            f: test_fn
        };
        let _c2 = converter.clone();
    }

    #[test]
    fn should_pass_into_different_tokio_spawns(){
        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build()
            .unwrap();
        let converter = PostgreConverter{
            f: test_fn
        };
        let c2 = converter.clone();
        let h1 = runtime.spawn(async move{
            let ic = c2.clone();
            let _f = ic.f;
        });
        let c3 = converter.clone();
        let h2 = runtime.spawn(async move{
            let ic = c3.clone();
            let _f = ic.f;
        });
        let _r = runtime.block_on(async move{
            let _r = h1.await;
            let _r2 = h2.await;
        });
    }
}
