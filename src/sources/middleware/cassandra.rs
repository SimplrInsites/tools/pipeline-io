//! The middleware task for Cassandra sources and sinks

use std::collections::HashMap;

use serde_json::Value;
use cdrs::types::prelude::Row;

/// Standard task composed of a single function
#[derive(Clone)]
pub struct CassandraConverter{
    pub f: fn(Row) -> Result<HashMap<String, Value>, String>
}


#[cfg(test)]
pub mod tests{
    use cdrs::types::rows::Row;
    use std::collections::HashMap;
    use serde_json::Value;
    use crate::sources::middleware::cassandra::CassandraConverter;

    /// Test the conversion
    fn test_convert(_r: Row) -> Result<HashMap<String, Value>, String>{
        Ok(HashMap::new())
    }

    /// Converter should clone
    #[test]
    fn should_clone(){
        let converter = CassandraConverter{
            f: test_convert
        };
        let _c2 = converter.clone();
    }

    /// Should pass between tokio spawns
    #[test]
    fn should_pass_between_tokio_spawns(){
        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build()
            .unwrap();
        let converter = CassandraConverter{
            f: test_convert
        };
        let c1 = converter.clone();
        let c2 = converter.clone();
        let h1 = runtime.spawn(async move{
            let ic = c1.clone();
            let _f = ic.f;
        });
        let h2 = runtime.spawn(async move{
            let ic = c2.clone();
            let _f = ic.f;
        });
        runtime.block_on(async move{
            let _r = h1.await;
            let _r2 = h2.await;
        })
    }
}