//! Postgre Source. Pagination occurs using a known offset column
//! for speed.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use chrono::{NaiveDateTime, ParseResult, Utc};
use serde_json::Value;
use tokio::sync::{Mutex, Semaphore};
use tokio_postgres::Row;

use crate::connection_factory::cassandra::IDType;
use crate::connection_factory::postgresql::PostgreSQLDataSource;
use crate::number::utils as number_utils;
use crate::query::utils;
use crate::records::batch::Batch;
use crate::sources::middleware::postgres::PostgreConverter;
use crate::sources::wait::{BatchWaiter, NotificationItem};
use crate::time::chrono_utils;
use tokio::time::Duration;
use crate::records::cache::RecordCache;

#[derive(Clone, Debug)]
pub enum StartingValue{
    NUMBER(i64),
    DATETIME(NaiveDateTime)
}


pub struct PostgreSource{
    pub sema: Arc<Semaphore>,
    data_source: PostgreSQLDataSource,
    columns: Vec<String>,
    pub notification_queue: Arc<Mutex<Vec<NotificationItem>>>,
    pub is_running: Arc<AtomicBool>,
    cache_size: i32,
    status: Arc<AtomicBool>,
    table: String,
    id_column: String,
    id_type: IDType,
    starting_value: StartingValue,
    converter: PostgreConverter,
    query_base: Option<String>
}


impl PostgreSource{

    /// Convert a datetime to a string
    ///
    /// # Variables
    /// * date_time -   The naive date time
    fn date_time_to_string(date_time: NaiveDateTime) -> String{
        date_time.to_string()
    }

    /// Parse the date time result
    ///
    /// # Variables
    /// * `date_str` - The date string to parse
    fn parse_date_time(date_str: String) -> ParseResult<NaiveDateTime>{
        NaiveDateTime::parse_from_str(date_str.as_str(), "%Y-%m-%d %H:%M:%S")
    }

    /// Create a Cassandra query for the data source. Query format is
    /// {query} WHERE {id_column} > {starting_value} LIMIT {limit}
    ///
    /// # Variables
    /// * `query` - Base query for cassandra
    /// * `id_column` - The id column
    /// * `starting_value` - The starting value
    /// * `limit` - Number of records to pull per query
    fn get_number_related_query(
        query: String,
        id_column: String,
        starting_value: i64,
        limit: usize) -> String{
        let starting_string = starting_value.to_string();
        let q = format!(
            "{} WHERE {} > {} ORDER BY {} ASC LIMIT {}",
            query,
            id_column,
            starting_string,
            id_column,
            limit.to_string());
        q
    }

    /// Create a Cassandra query for the data source. Query format is
    /// {query} WHERE {id_column} < {starting_value} LIMIT {limit}
    ///
    /// # Variables
    /// * `query` - Base query for cassandra
    /// * `id_column` - The id column
    /// * `starting_value` - The starting value
    /// * `limit` -  Limit
    fn get_date_related_query(
        query: String, id_column: String, starting_value: NaiveDateTime, limit: usize) -> String{
        let starting_string = PostgreSource::date_time_to_string(
            starting_value);
        let q = format!(
            "{} WHERE {} > {} ORDER BY {} ASC LIMIT {} ",
            query,
            id_column,
            starting_string,
            id_column,
            limit.to_string());
        q
    }

    /// Get the related query.
    ///
    /// # Variable
    /// * `query` - The query to run
    /// * `id_column` - ID column name
    /// * `starting_value` - The starting value
    /// * `id_type` - Type of the id
    /// * `limit` - The limit for the queries
    fn get_query(
        query: String,
        id_column: String,
        starting_value: StartingValue,
        id_type: IDType,
        limit: usize) -> String{
        match id_type{
            IDType::Number =>{
                match starting_value {
                    StartingValue::NUMBER(starting_value) =>{
                        PostgreSource::get_number_related_query(
                            query, id_column, starting_value, limit)
                    },
                    StartingValue::DATETIME(_starting_value) =>{
                        panic!("Starting Value for Cassandra Must be Number")
                    }
                }
            },
            IDType::DateTime =>{
                match starting_value {
                    StartingValue::NUMBER(_starting_value) =>{
                        panic!("Starting Value for Cassandra Must be DateTime")
                    },
                    StartingValue::DATETIME(starting_value) =>{
                        PostgreSource::get_date_related_query(
                            query, id_column, starting_value, limit)
                    }
                }
            }
        }
    }

    /// Wait for the next batch of records. Returns a condition to wait on.
    /// When notified, call next.
    pub async fn queue_for_next(sema: Arc<Semaphore>,
                                notification_queue: Arc<Mutex<Vec<NotificationItem>>>) -> Arc<Mutex<BatchWaiter>>{
        let (waiter, notifier) = BatchWaiter::new();
        let mut mgaurd = notification_queue.lock().await;
        mgaurd.push(notifier);
        sema.add_permits(1);
        Arc::new(Mutex::new(waiter))
    }

    /// Get the starting date value
    ///
    /// # Variables
    /// * `records` - The records
    fn get_date_starting_value(records: &Vec<HashMap<String, Value>>, id_column: &str) -> StartingValue{
        let mut kv  = vec![];
        for i in 0..records.len() {
            let record = records.get(i).unwrap();
            let v = record.get(id_column).unwrap();
            let s = v.as_str().unwrap().to_string();
            let dt = PostgreSource::parse_date_time(s).unwrap();
            kv.push((i, dt));
        }
        let (_idx, ndt) = chrono_utils::get_max_date(kv);
        StartingValue::DATETIME(ndt)
    }

    /// Get the starting number value
    ///
    /// # Variables
    /// * `records` - The records
    fn get_number_starting_value(
        records: &Vec<HashMap<String, Value>>, id_column: &str) -> StartingValue{
        let mut kv  = vec![];
        for i in 0..records.len() {
            let record = records.get(i).unwrap();
            let v = record.get(id_column);
            let s = v.unwrap().as_i64().unwrap();
            kv.push((i, s));
        }
        let (_idx, max_n) = number_utils::get_max_i64(kv);
        StartingValue::NUMBER(max_n)
    }

    /// Update the setarting record value
    ///
    /// # Variable
    /// * `starting_value` - The starting value
    /// * `records` - Records to update
    pub fn update_starting_record(
        starting_value: StartingValue, records: &Vec<HashMap<String, Value>>, id_column: &str) -> StartingValue{
        match starting_value.clone() {
            StartingValue::DATETIME(_starting_value) => {
                PostgreSource::get_date_starting_value(records, id_column)
            },
            StartingValue::NUMBER(_starting_value) => {
                PostgreSource::get_number_starting_value(records, id_column)
            }
        }
    }

    /// Get the batch from PostgreSQL rows.
    ///
    /// # Variable
    /// * `records` - Rows from PostgreSQL
    /// * `starting_value` - The starting value
    /// * `convert` - Record converter
    /// * `id_column` - name of the id column
    fn get_batch(
        records: Vec<Row>,
        starting_value: StartingValue,
        converter: PostgreConverter,
        id_column: &str) -> Result<(Batch, StartingValue), ()>{
        if records.len() == 0{
            Ok((Batch::new(vec![]), starting_value))
        }else{
            let mut batch_recs = vec![];
            for record in records{
                let f = converter.f;
                let r = f(record);
                if r.is_ok(){
                    batch_recs.push(r.unwrap());
                }
            }
            let mut new_start = starting_value.clone();
            new_start = PostgreSource::update_starting_record(
                new_start, &batch_recs, id_column);
            let batch = Batch::new(batch_recs);
            Ok((batch, new_start))
        }
    }

    /// Pull records from the source
    ///
    /// # Variables
    async fn pull_records(is_running: Arc<AtomicBool>,
                          status: Arc<AtomicBool>,
                          sema: Arc<Semaphore>,
                          data_source: PostgreSQLDataSource,
                          query: String,
                          id_column: String,
                          limit: usize,
                          starting_value: StartingValue,
                          id_type: IDType,
                          converter: PostgreConverter,
                          notification_queue: Arc<Mutex<Vec<NotificationItem>>>){
        let mut new_start = starting_value.clone();
        let mut ds = data_source;
        let mut failures: i32 = 0;
        let mut run = true;
        let mut cache = RecordCache::new();
        let mut timestamp = Utc::now();
        if ds.is_instantiated() == false{
            ds.build().await;
        }
        while status.load(Ordering::Relaxed) && run{
            let is_alive = ds.is_alive().await;
            if is_alive == false{
                let _r = ds.build().await;
            }
            let batch_result = {
                if cache.size() < (limit * 3) {
                    let source = ds.get_connection();
                    let mguard = source.lock().await;
                    let q = PostgreSource::get_query(
                        query.clone(),
                        id_column.clone(),
                        new_start.clone(),
                        id_type.clone(),
                        limit.clone());
                    let r = mguard.client.query(q.as_str(), &[]).await;
                    let rows = r.unwrap();
                    drop(mguard);
                    if rows.len() > 0 {
                        let batch = PostgreSource::get_batch(
                            rows,
                            starting_value.clone(),
                            converter.clone(),
                            id_column.as_str());
                        if batch.is_ok() {
                            let (cache_rows, starting_value) = batch.unwrap();
                            cache.add_records(cache_rows.get_records());
                            let batch = cache.get_n_records(limit);
                            Ok((batch, starting_value.clone()))
                        }else{
                            Err(())
                        }
                    } else if cache.size() > 0 {
                        let records = cache.get_records();
                        Ok((Batch::new(records), starting_value.clone()))
                    }else{
                        Ok((Batch::new(vec![]), starting_value.clone()))
                    }
                } else {
                    Ok((cache.get_n_records(limit), starting_value.clone()))
                }
            };
            if batch_result.is_ok(){
                let now = Utc::now();
                let since_push = now.signed_duration_since(timestamp);
                let delta_s = since_push.num_seconds();
                let (batch, start) = batch_result.unwrap();
                if batch.size() > 0 {
                    cache.add_records(batch.get_records());
                    failures = 0;
                    if (cache.size() >= limit || delta_s > 5) && cache.size() > 0 {
                        let out_batch = cache.get_n_records(limit as usize);
                        let rf = sema.acquire();
                        let res = tokio::time::timeout(
                            Duration::new(0, 750), rf).await;
                        if res.is_ok() {
                            let permit = res.unwrap();
                            permit.forget();
                            let mut nguard = notification_queue.lock().await;
                            if nguard.len() > 0 {
                                let v = nguard.pop().unwrap();
                                new_start = start;
                                v.send_batch(out_batch);
                            }
                            timestamp = Utc::now();
                            drop(nguard);
                        } else {
                            cache.append_to_front(out_batch);
                        }
                    } else if cache.size() == 0 {
                        is_running.store(false, Ordering::Relaxed);
                        run = false;
                    }
                }else{
                    run = false;
                }
            }else{
                failures += 1;
                if failures > 3{
                    panic!("Too many failures in PostgreSQL Source")
                }
            }
        }
    }

    /// Start the source. The ordering of your columns is important.
    pub async fn run(&mut self){
        let is_running = self.is_running.clone();
        let status = self.status.clone();
        let notification_queue = self.notification_queue.clone();
        let id_type = self.id_type.clone();
        let converter = self.converter.clone();
        let sema = self.sema.clone();
        let mut data_source = self.data_source.clone();
        data_source.build().await;
        let columns = self.columns.clone();
        let limit = self.cache_size.clone() as usize;
        let table = self.table.clone();
        let id_column = self.id_column.clone();
        let starting_value = self.starting_value.clone();
        let column_str = utils::columns_to_string(columns.clone());
        let query = {
            if self.query_base.is_none() {
                format!("SELECT {} FROM {}", column_str, table)
            }else{
                self.query_base.clone().unwrap()
            }
        };
        PostgreSource::pull_records(
            is_running,
            status,
            sema,
            data_source,
            query,
            id_column,
            limit,
            starting_value,
            id_type,
            converter,
            notification_queue
        ).await;
    }

    /// Wait for the application to close
    pub async fn close(&mut self){
        self.sema.add_permits(1);
    }

    /// Create a new PostgreSource
    ///
    /// # Variables
    /// * `data_source` - The postgresql data source
    /// * `columns` - Columns to limit to in the query
    /// * `cache_size` - Target size of the cache which may be exceeded by 3 times the size
    /// * `status` - Status of the application
    /// * `table` - Table to pull from
    /// * `id_column` - Id column
    /// * `starting_value` - Starting value for the query
    /// * `converter` - Converts rows
    /// * `query_base` - Overrides the default query and must be compatible with an additional where and limit clause
    pub fn new(
        data_source: PostgreSQLDataSource,
        columns: Vec<String>,
        cache_size: i32,
        status: Arc<AtomicBool>,
        table: String,
        id_column: String,
        id_type: IDType,
        starting_value: StartingValue,
        converter: PostgreConverter,
        query_base: Option<String>) -> PostgreSource{
        PostgreSource{
            sema: Arc::new(Semaphore::new(0)),
            data_source,
            columns,
            notification_queue: Arc::new(Mutex::new(vec![])),
            is_running: Arc::new(AtomicBool::new(true)),
            cache_size,
            status,
            table,
            id_column,
            id_type,
            starting_value,
            converter,
            query_base
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};

    use serde_json::{Value, Number};
    use tokio_postgres::Row;

    use crate::connection_factory::cassandra::IDType;
    use crate::connection_factory::postgresql::PostgreSQLDataSource;
    use crate::sources::postgres::{PostgreSource, StartingValue};
    use crate::sources::middleware::postgres::PostgreConverter;
    use crate::records::batch::Batch;

    fn convert_rows(r: Row) -> Result<HashMap<String, Value>, String>{
        let mut record = HashMap::<String, Value>::new();
        let columns = r.columns();
        let test_id = "test_id".to_string();
        let tnum = "test_num".to_string();
        let tstr = "test_str".to_string();
        for i in 0..columns.len(){
            let c = columns.get(i).unwrap();
            let cname = c.name();
            match cname{
                "test_id" =>{
                    let v: i64 = r.get(i);
                    record.insert(
                        test_id.clone(), Value::Number(Number::from(v)));
                },
                "test_num" =>{
                    let v: i32 = r.get(i);
                    record.insert(
                        tnum.clone(), Value::Number(Number::from(v)));
                },
                "test_str" =>{
                    let v: &str = r.get(i);
                    record.insert(
                        tstr.clone(), Value::String(v.to_string()));
                },
                _ =>{
                    panic!("Column name {} does not exist", cname);
                }
            }
        }
        Ok(record)
    }

    fn get_vars() -> (String, u16, String, String, String){
        let host = std::env::var("postgresql_host").unwrap();
        let port_string = std::env::var("postgresql_port").unwrap();
        let db = std::env::var("postgresql_db").unwrap();
        let user = std::env::var("postgresql_user").unwrap();
        let pwd = std::env::var("postgresql_pwd").unwrap();
        let port = u16::from_str_radix(&port_string, 10).unwrap();
        (host, port, db, user, pwd)
    }

    async fn truncate_table(ds: &mut PostgreSQLDataSource){
        let conn = ds.get_connection();
        let mguard = conn.lock().await;
        let q = mguard.client.prepare(
            "TRUNCATE TABLE test.dc_test_table").await;
        let stmt = q.unwrap();
        let r = mguard.client.execute(&stmt, &[]).await;
        assert!(r.is_ok());
    }

    async fn insert_test_records(ds: &mut PostgreSQLDataSource){
        let conn = ds.get_connection();
        let mut mguard = conn.lock().await;
        let tres = mguard.client.transaction().await;
        let transaction = tres.unwrap();
        let stmtr = transaction.prepare(
            "INSERT INTO test.dc_test_table (test_str, test_num) VALUES($1, $2)").await;
        let stmt = stmtr.unwrap();
        let _f = transaction.execute(&stmt, &[&"test_string", &1]).await;
        let _f = transaction.execute(&stmt, &[&"test_string2", &2]).await;
        let f = transaction.commit().await;
        assert!(f.is_ok());
    }

    fn get_data_source() -> PostgreSQLDataSource{
        let (host, port, db, user, pwd) = get_vars();
        let ds = PostgreSQLDataSource::new(
            host, port, db, user, pwd);
        ds
    }

    /// Obtaining the source
    fn get_source(ds: PostgreSQLDataSource, limit: i32) -> (Arc<AtomicBool>, PostgreSource){
        let table = "test.dc_test_table";
        let idtype = IDType::Number;
        let status = Arc::new(AtomicBool::new(true));
        let row_converter = PostgreConverter{
            f: convert_rows
        };
        let source = PostgreSource::new(
            ds,
            vec![],
            limit,
            status.clone(),
            table.to_string(),
            "test_id".to_string(),
            idtype,
            StartingValue::NUMBER(0),
            row_converter.clone(),
        None);
        (status, source)
    }

    #[test]
    fn should_create_source(){
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        rt.block_on(async move {
            let mut ds = get_data_source();
            let _r = ds.build().await;
            let (status, mut source) = get_source(ds, 2);
            let h1 = tokio::spawn(async move {
                let _r = source.run().await;
            });
            status.store(true, Ordering::Relaxed);
            let _r = h1.await;
        });
    }

    #[test]
    fn should_get_batch(){
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        rt.block_on(async move {
            let mut ds = get_data_source();
            ds.build().await;
            insert_test_records(&mut ds).await;
            insert_test_records(&mut ds).await;
            insert_test_records(&mut ds).await;
            insert_test_records(&mut ds).await;
            let (status, mut source) = get_source(ds, 2);
            let sema = source.sema.clone();
            let nq = source.notification_queue.clone();
            let h1 = tokio::spawn(async move {
                let _r = source.run().await;
            });
            let h2 = tokio::spawn(async move{
                let mut bopt = None;
                for _i  in 0..3 as usize {
                    let waiter = PostgreSource::queue_for_next(
                        sema.clone(), nq.clone()).await;
                    let mut mwait = waiter.lock().await;
                    let r = mwait.receive().await;
                    let b = r.unwrap();
                    let mut recs = b.get_records();
                    if bopt.is_none(){
                        let b = Batch::new(recs);
                        bopt = Some(b);
                    }else{
                        let b = bopt.unwrap();
                        let mut orecs = b.get_records();
                        orecs.append(&mut recs);
                        bopt = Some(Batch::new(orecs));
                    }
                }
                let r = bopt.unwrap();
                r
            });
            let br = h2.await;
            let mut b = br.unwrap();
            status.store(false, Ordering::Relaxed);
            let _r = h1.await;
            let mut ds2 = get_data_source();
            ds2.build().await;
            truncate_table(&mut ds2).await;
            assert_eq!(b.size(), 6);
            let mut bopt = b.next();
            while bopt.is_some(){
                let rec = bopt.unwrap();
                assert!(rec.get("test_id").is_some());
                assert!(rec.get("test_str").is_some());
                let test_str_opt = rec.get("test_str").unwrap();
                let test_str = test_str_opt.as_str().unwrap();
                assert!(test_str.contains("test_string"));
                bopt = b.next();
            }
            status.store(true, Ordering::Relaxed);
        });
    }
}