//! Cassandra source. Pulls records from salesforce using signals.
//!
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use chrono::NaiveDateTime;
use regex::Regex;
use tokio::sync::{Mutex, Semaphore};
use tokio::time::Duration;

use crate::connection_factory::cassandra::{CassandraDataSource, IDType};
use crate::records::batch::Batch;
use crate::records::cache::RecordCache;
use crate::sources::middleware::cassandra::CassandraConverter;
use crate::sources::wait::{BatchWaiter, NotificationItem};

/// Starting value for queries
#[derive(Clone, Debug)]
pub enum StartingValue{
    NUMBER(u32),
    DATETIME(NaiveDateTime)
}



/// Cassandra Source struct for pulling and processing records
///
/// # Variables
/// * `data_source` - The Cassandra data source
pub struct CassandraSource{
    pub sema: Arc<Semaphore>,
    data_source: CassandraDataSource,
    columns: Vec<String>,
    pub notification_queue: Arc<Mutex<Vec<NotificationItem>>>,
    pub is_running: Arc<AtomicBool>,
    emmit_size: i32,
    status: Arc<AtomicBool>,
    table: String,
    id_column: String,
    id_type: IDType,
    starting_value: StartingValue,
    converter: CassandraConverter
}


impl CassandraSource{

    /// Convert a datetime to a string
    ///
    /// # Variables
    /// * date_time -   The naive date time
    fn date_time_to_string(date_time: NaiveDateTime) -> String{
        let tstr = date_time.to_string();
        let re = Regex::new(r"\..*").unwrap();
        let str_result = re.replace(tstr.as_str(), "");
        str_result.to_string()
    }

    /// Create a Cassandra query for the data source. Query format is
    /// {query} WHERE {id_column} > {starting_value} LIMIT {limit}
    ///
    /// # Variables
    /// * `query` - Base query for cassandra
    /// * `id_column` - The id column
    /// * `starting_value` - The starting value
    fn get_number_related_query(
        query: String, id_column: String, starting_value: u32) -> String{
        let starting_string = starting_value.to_string();
        let q = format!(
            "{} WHERE {} > {}", query, id_column, starting_string);
        q
    }

    /// Create a Cassandra query for the data source. Query format is
    /// {query} WHERE {id_column} < {starting_value} LIMIT {limit}
    ///
    /// # Variables
    /// * `query` - Base query for cassandra
    /// * `id_column` - The id column
    /// * `starting_value` - The starting value
    fn get_date_related_query(
        query: String, id_column: String, starting_value: NaiveDateTime) -> String{
        let starting_string = CassandraSource::date_time_to_string(
            starting_value);
        let q = format!(
            "{} WHERE {} > '{}' ", query, id_column, starting_string);
        q
    }

    /// Get the query. Be aware that the cache size is not likely to be the
    /// total number of records due to pagination. Cache size is the pagination size.
    ///
    /// # variable
    /// * `query` - Base query
    /// * `id_column` - Cassandra id column
    /// * `starting_value` - Starting value
    /// * `id_type` - Type of ID
    fn get_query(
        query: String, id_column: String, starting_value: StartingValue, id_type: IDType) -> String{
        match id_type{
            IDType::Number =>{
                match starting_value {
                    StartingValue::NUMBER(starting_value) =>{
                        CassandraSource::get_number_related_query(
                            query, id_column, starting_value)
                    },
                    StartingValue::DATETIME(_starting_value) =>{
                        panic!("Starting Value for Cassandra Must be Number")
                    }
                }
            },
            IDType::DateTime =>{
                match starting_value {
                    StartingValue::NUMBER(_starting_value) =>{
                        panic!("Starting Value for Cassandra Must be DateTime")
                    },
                    StartingValue::DATETIME(starting_value) => {
                        CassandraSource::get_date_related_query(
                            query, id_column, starting_value)
                    }
                }
            }
        }
    }

    /// Start Pulling Records
    async fn pull_records(
        is_running: Arc<AtomicBool>,
        status: Arc<AtomicBool>,
        sema: Arc<Semaphore>,
        data_source: CassandraDataSource,
        query: String,
        id_column: String,
        emmit_size: i32,
        starting_value: StartingValue,
        id_type: IDType,
        converter: CassandraConverter,
        notification_queue: Arc<Mutex<Vec<NotificationItem>>>){
        let mut ds = data_source;
        let source_opt = ds.get_connection().await;
        if source_opt.is_some(){
            let f = converter.f;
            let mut cache = RecordCache::new();
            let session = source_opt.unwrap();
            let q = CassandraSource::get_query(
            query, id_column, starting_value, id_type);
            let limit = emmit_size * 5;
            let npull: f32 = emmit_size as f32 * 1.5;
            let pull_at = npull.floor() as usize;
            let mgaurd = session.lock().await;
            let mut pager = mgaurd.paged(limit);
            let mut query_pager = pager.query(q);
            let mut next_rows = query_pager.next();
            if next_rows.is_err(){
                panic!("Cassandra Query Failed");
            }else {
                while status.load(Ordering::Relaxed) && (next_rows.is_ok() || cache.size() > 0) {
                    let records = {
                        if cache.size() <= pull_at && query_pager.has_more() {
                            if next_rows.is_ok() {
                                let mut batch_rows = vec![];
                                let batch_pgrows = next_rows.unwrap();
                                for row in batch_pgrows {
                                    let cache_rowr = f(row);
                                    if cache_rowr.is_ok() {
                                        let cache_row = cache_rowr.unwrap();
                                        batch_rows.push(cache_row);
                                    }
                                }
                                cache.add_records(batch_rows);
                                let batch = cache.get_n_records(emmit_size as usize);
                                next_rows = query_pager.next();
                                batch
                            } else {
                                let rows = cache.get_n_records(emmit_size as usize);
                                rows
                            }
                        } else if cache.size() > 0 {
                            let rows = cache.get_n_records(emmit_size as usize);
                            rows
                        } else {
                            Batch::new(vec![])
                        }
                    };
                    if records.size() > 0 {
                        let sema_wait = sema.acquire();
                        let permit_result = tokio::time::timeout(
                            Duration::new(0, 750), sema_wait).await;
                        if permit_result.is_ok() {
                            let permit = permit_result.unwrap();
                            permit.forget();
                            let mut nguard = notification_queue.lock().await;
                            let v = nguard.pop();
                            if v.is_some() {
                                let item = v.unwrap();
                                item.send_batch(records);
                            }
                            drop(nguard);
                        }
                    }
                }
                is_running.store(false, Ordering::Relaxed);
                drop(mgaurd)
            }
        }else{
            panic!("Data Source Undefined");
        }
    }

    /// Wait for the next batch of records. Returns a condition
    /// to wait on. When notified, call next.
    pub async fn queue_for_next(
        sema: Arc<Semaphore>,
        notification_queue: Arc<Mutex<Vec<NotificationItem>>>) -> Arc<Mutex<BatchWaiter>>{
        let (waiter, notifier) = BatchWaiter::new();
        let mut mgaurd = notification_queue.lock().await;
        mgaurd.push(notifier);
        sema.add_permits(1);
        Arc::new(Mutex::new(waiter))
    }

    /// Convert columns to string
    ///
    /// # Variables
    /// * `columns` - The columns
    fn columns_to_string(columns: Vec<String>) -> String{
        if columns.len() > 0{
            let mut column_str = "".to_string();
            for column in columns.clone(){
                if column_str.len() == 0{
                    column_str = column;
                }else{
                    column_str = format!("{}, {}", column_str, column);
                }
            }
            column_str
        }else{
            "*".to_string()
        }
    }

    /// Start the source. The ordering of your columns is important.
    pub async fn run(&mut self){
        let is_running = self.is_running.clone();
        let status = self.status.clone();
        let sema = self.sema.clone();
        let data_source = self.data_source.clone();
        let id_column = self.id_column.clone();
        let emmit_size = self.emmit_size.clone();
        let starting_value = self.starting_value.clone();
        let id_type = self.id_type.clone();
        let converter = self.converter.clone();
        let notification_queue = self.notification_queue.clone();
        let column_str = CassandraSource::columns_to_string(self.columns.clone());
        let query = format!("SELECT {} FROM {}", column_str, self.table);
        CassandraSource::pull_records(
            is_running,
            status,
            sema,
            data_source,
            query,
            id_column,
            emmit_size,
            starting_value,
            id_type,
            converter,
            notification_queue
        ).await;
    }

    /// Create a new Cassandra Data Source
    ///
    /// # Variable
    /// * `data_source` -  The data source
    /// * `emmit_size` - Size of the backing cache
    /// * `status` - Status of the application in general
    pub fn new(
        data_source: CassandraDataSource,
        columns: Vec<String>,
        emmit_size: i32,
        status: Arc<AtomicBool>,
        table: String,
        id_column: String,
        id_type: IDType,
        starting_value: StartingValue,
        converter: CassandraConverter) -> CassandraSource{
        let sema = Arc::new(Semaphore::new(0));
        CassandraSource{
            sema,
            data_source,
            columns,
            notification_queue: Arc::new(Mutex::new(vec![])),
            is_running: Arc::new(AtomicBool::new(true)),
            emmit_size,
            status,
            table,
            id_column,
            id_type,
            starting_value,
            converter
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::env;
    use std::ops::Sub;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};

    use cdrs::frame::IntoBytes;
    use cdrs::types::prelude::{Row, TryFromRow};
    use chrono::{Duration, Utc};
    use serde_json::Value;
    use tokio::runtime::{Builder, Runtime};

    use crate::cdrs::types::from_cdrs::FromCDRSByName;
    use crate::connection_factory::cassandra::{CassandraDataSource, IDType};
    use crate::sources::cassandra::{CassandraSource, StartingValue};
    use crate::sources::middleware::cassandra::CassandraConverter;
    use cdrs::types::IntoRustByName;

    #[derive(Clone, Debug, IntoCDRSValue, TryFromRow, PartialEq)]
    pub struct RowStruct{
        insert_date: i64,
        scrape_page_hash: String,
        data: String
    }


    pub fn convert_from_cassandra(row: Row) -> Result<HashMap<String, Value>, String>{
        let insert_date: Option<i64> = row.get_by_name("insert_date").unwrap();
        let scrape_page_hash: Option<String> = row.get_by_name("scrape_page_hash").unwrap();
        let data: Option<String> = row.get_by_name("data").unwrap();
        let mut v = HashMap::new();
        let data = data.unwrap();
        let json_data = serde_json::from_str(data.as_str()).unwrap();
        v.insert(
            "insert_date".to_string(), serde_json::to_value(insert_date.unwrap()).unwrap());
        v.insert(
            "scrape_page_hash".to_string(), serde_json::to_value(scrape_page_hash.unwrap()).unwrap());
        v.insert("data".to_string(), Value::Object(json_data));
        Ok(v)
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        Arc::new(rt)
    }

    fn get_env_vars() -> (String, u16, String, String){
        let host = env::var("cassandra_host").unwrap();
        let port_string = env::var("cassandra_port").unwrap();
        let user = env::var("cassandra_user").unwrap();
        let pwd = env::var("cassandra_pwd").unwrap();
        let port = u16::from_str_radix(&port_string, 10).unwrap();
        (host, port, user, pwd)
    }

    fn get_source() -> (CassandraSource, Arc<AtomicBool>){
        let now = Utc::now().sub(Duration::days(32));
        let (host, port, user, pwd) = get_env_vars();
        let table = "selective_scrapers.healthecareers_full_jobs".to_string();
        let id_column = "insert_date".to_string();
        let status = Arc::new(AtomicBool::new(true));
        let ds = CassandraDataSource::new(
            host, port, user, pwd);
        let starting_date = now.sub(Duration::days(30));
        let sv = StartingValue::DATETIME(starting_date.naive_utc());
        let columns = vec!["insert_date".to_string(), "scrape_page_hash".to_string(), "data".to_string()];
        let converter = CassandraConverter{
            f: convert_from_cassandra
        };
        let cassandra = CassandraSource::new(
            ds,
            columns,
            1000,
            status.clone(),
            table,
            id_column,
            IDType::DateTime,
            sv,
            converter
        );
        (cassandra, status)
    }

    #[test]
    fn should_create_source(){
        let (mut cassandra, status) = get_source();
        let rt = get_runtime();
        rt.block_on(async move{
            let h1 = tokio::spawn(async move{
                cassandra.run().await;
            });
            status.store(false, Ordering::Relaxed);
            let r = h1.await;
            assert!(r.is_ok());
        })
    }

    #[test]
    fn should_get_batch(){
        let (mut cassandra, status) = get_source();
        let sema = cassandra.sema.clone();
        let nq = cassandra.notification_queue.clone();
        let rt = get_runtime();
        rt.block_on(async move{
            let h1 = tokio::spawn(async move{
                cassandra.run().await;
            });
            let h2 = tokio::spawn(async move{
                for _i in 0..3 {
                    let waiter = CassandraSource::queue_for_next(
                        sema.clone(), nq.clone()).await;
                    let mut mguard = waiter.lock().await;
                    let batch_result = mguard.receive().await;
                    let batch = batch_result.unwrap();
                    assert!(batch.size() > 0);
                }
            });
            let _r = h2.await;
            status.store(false, Ordering::Relaxed);
            let _r = h1.await;
        })
    }
}
