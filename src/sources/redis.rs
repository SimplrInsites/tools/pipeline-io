//! A redis source. Builds up to cache until the user requests a release.
//! It is possible for the cache to grow to the size of the input so
//! this source could take quite a bit of RAM or disk.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::sync::Arc;

use tokio::sync::{Mutex, Semaphore, Notify};

use crate::sources::wait::{NotificationItem, BatchWaiter};
use std::sync::atomic::{AtomicBool, Ordering};
use futures::StreamExt;
use redis_async::resp::FromResp;
use serde_json::Value;
use std::collections::HashMap;
use chrono::Utc;
use std::cmp::min;
use crate::records::batch::Batch;
use crate::records::cache::RecordCache;
use crate::connection_factory::redis_connection_utilities::create_pubsub_conn;
use tokio::time::Duration;


/// Redis Source for streaming messages
#[derive(Clone)]
pub struct RedisSource{
    status: Arc<AtomicBool>,
    route: String,
    url: String,
    pub sema: Arc<Semaphore>,
    batch_size: usize,
    pub notification_queue: Arc<Mutex<Vec<NotificationItem>>>
}


impl RedisSource{

    /// Run the source
    pub async fn run(&self, start_notify: Arc<Notify>){
        let sema = self.sema.clone();
        let notification_queue = self.notification_queue.clone();
        let batch_size = self.batch_size.clone();
        let r = create_pubsub_conn(self.url.clone()).await;
        let conn = r.unwrap();
        let stream_r = conn.subscribe(self.route.as_str()).await;
        let mut stream = stream_r.unwrap();
        start_notify.notify_one();
        let mut timestamp = Utc::now();
        let mut cache = RecordCache::new();
        while self.status.load(Ordering::Relaxed) {
            let redisf = stream.next();
            let mr = tokio::time::timeout(
                Duration::new(0, 750), redisf).await;
            if mr.is_ok() {
                let m = mr.unwrap();
                let resp_result = m.unwrap();
                if resp_result.is_ok() {
                    let resp_value = resp_result.unwrap();
                    let json_r = String::from_resp(resp_value);
                    if json_r.is_ok() {
                        let json_str = json_r.unwrap();
                        let json_result = serde_json::from_str(json_str.as_str());
                        let json: Vec<HashMap<String, Value>> = json_result.unwrap();
                        cache.add_records(json);
                    }
                }
            }
            let now = Utc::now();
            let since_push = now.signed_duration_since(timestamp);
            let delta_s = since_push.num_seconds();
            if (cache.size() >= batch_size || delta_s > 5) && cache.size() > 0{
                let sz = min(cache.size(), batch_size);
                if sz > 0 {
                    let permit = sema.acquire().await;
                    permit.forget();
                    let records = cache.get_records();
                    let (v, v2) = records.split_at(sz);
                    cache.update_records(v2.to_vec());
                    let mut nguard = notification_queue.lock().await;
                    if nguard.len() > 0 {
                        let item = nguard.pop().unwrap();
                        let b = Batch::new(v.to_vec());
                        item.send_batch(b);
                    }
                    drop(nguard);
                }
                timestamp = Utc::now();
            }
        }
    }

    /// Wait for the next batch of records. Returns a condition to wait on.
    /// When notified, call next.
    pub async fn queue_for_next(
        sema: Arc<Semaphore>,
        notification_queue: Arc<Mutex<Vec<NotificationItem>>>) -> Arc<Mutex<BatchWaiter>>{
        let (waiter, notifier) = BatchWaiter::new();
        let mut mgaurd = notification_queue.lock().await;
        mgaurd.push(notifier);
        sema.add_permits(1);
        Arc::new(Mutex::new(waiter))
    }

    /// A Redis consumer
    ///
    /// # Arguments
    /// * `status` - Status of the application
    /// * `url` - The url for the connection
    /// * `route` - Subscription route
    /// * `batch_size` - The target batch size
    pub fn new(
        status: Arc<AtomicBool>, url: String, route: String, batch_size: usize) -> RedisSource {
        RedisSource{
            status,
            route,
            url,
            sema: Arc::new(Semaphore::new(0)),
            notification_queue: Arc::new(Default::default()),
            batch_size
        }
    }
}


#[cfg(test)]
pub mod tests{
    use crate::sources::redis::RedisSource;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};
    use crate::connection_factory::redis_connection_utilities;
    use std::collections::HashMap;
    use serde_json::Value;
    use redis_async::{error, resp_array};
    use redis_async::resp::RespValue;
    use tokio::sync::Notify;

    /// Get test environment variables
    fn get_vars() -> (String, String, i64){
        let host = std::env::var("REDISHOST").unwrap();
        let port_string = std::env::var("REDISPORT").unwrap();
        let redis_uri = format!("{}:{}", host.clone(), port_string);
        (redis_uri, host, i64::from_str_radix(port_string.as_str(), 10).unwrap())
    }

    /// Get test records
    fn get_records() -> Vec<HashMap<String, Value>>{
        let mut records = vec![];
        let mut record = HashMap::new();
        record.insert("test".to_string(), Value::Bool(true));
        record.insert("test_str".to_string(), Value::String("Hello World!".to_string()));
        let mut record2 = HashMap::new();
        record2.insert("test".to_string(), Value::Bool(true));
        record2.insert("test_str".to_string(), Value::String("Hello World!".to_string()));
        records.push(record);
        records.push(record2);
        records
    }

    #[test]
    fn should_receive_records(){
        let (uri, _host, _port) = get_vars();
        let status = Arc::new(AtomicBool::new(true));
        let source_status = status.clone();
        let source = RedisSource::new(
            source_status, uri.clone(), "test".to_string(), 2);
        let sema = source.sema.clone();
        let notify_q = source.notification_queue.clone();
        let source_arc = Arc::new(source);
        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(10).build().unwrap();
        let aruntime = Arc::new(runtime);
        let notify = Arc::new(Notify::new());
        let arc_notify = notify.clone();
        let h1 = aruntime.clone().spawn(async move{
            let _r = source_arc.run(arc_notify).await;
        });
        let addr = uri.clone();
        let h2 = aruntime.clone().spawn(async move{
            notify.notified().await;
            let publisher = redis_connection_utilities::create_paired_conn(
                addr).await;
            let records = get_records();
            let json_str = serde_json::to_string(&records).unwrap();
            let _r: Result<RespValue, error::Error> = publisher.send(
                resp_array!["PUBLISH", "test", json_str.as_str()]).await;
            let waiter = RedisSource::queue_for_next(
                sema, notify_q).await;
            let mut mguard = waiter.lock().await;
            let waiter_result = mguard.receive().await;
            assert!(waiter_result.is_ok());
            let b = waiter_result.unwrap();
            assert_eq!(b.size(), 2);
            status.store(false, Ordering::Relaxed);
        });
        aruntime.block_on(async move {
            let _r = h1.await;
            let _r2 = h2.await;
        });
    }

    #[test]
    fn should_send_records(){
        let (uri, _host, _port) = get_vars();
        let status = Arc::new(AtomicBool::new(true));
        let source_status = status.clone();
        let source = RedisSource::new(
            source_status, uri.clone(), "test".to_string(), 100);
        let source_arc = Arc::new(source);
        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        let notify = Arc::new(Notify::new());
        let arc_notify = notify.clone();
        let h1 = runtime.spawn(async move{
            let _r = source_arc.run(notify).await;
        });
        runtime.block_on(async move {
            arc_notify.notified().await;
            let addr = uri.clone();
            let publisher = redis_connection_utilities::create_paired_conn(
                addr).await;
            let records = get_records();
            let json_str = serde_json::to_string(&records).unwrap();
            let _r: Result<RespValue, error::Error> = publisher.send(
                resp_array!["PUBLISH", "test", json_str.as_str()]).await;
            status.store(false, Ordering::Relaxed);
            let _r = h1.await;
        });
    }

    #[test]
    fn should_emit_records_after_n_seconds(){
        let (uri, _host, _port) = get_vars();
        let status = Arc::new(AtomicBool::new(true));
        let source_status = status.clone();
        let source = RedisSource::new(
            source_status, uri.clone(), "test".to_string(), 3);
        let sema = source.sema.clone();
        let notify_q = source.notification_queue.clone();
        let source_arc = Arc::new(source);
        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(10).build().unwrap();
        let aruntime = Arc::new(runtime);
        let notify = Arc::new(Notify::new());
        let arc_notify = notify.clone();
        let h1 = aruntime.clone().spawn(async move{
            let _r = source_arc.run(arc_notify).await;
        });
        let h2 = aruntime.clone().spawn(async move{
            notify.notified().await;
            let addr = uri.clone();
            let publisher = redis_connection_utilities::create_paired_conn(
                addr).await;
            let records = get_records();
            let json_str = serde_json::to_string(&records).unwrap();
            let _r: Result<RespValue, error::Error> = publisher.send(
                resp_array!["PUBLISH", "test", json_str.as_str()]).await;
            let waiter = RedisSource::queue_for_next(
                sema, notify_q).await;
            let mut mguard = waiter.lock().await;
            let waiter_result = mguard.receive().await;
            assert!(waiter_result.is_ok());
            let b = waiter_result.unwrap();
            assert_eq!(b.size(), 2);
            status.store(false, Ordering::Relaxed);
        });
        aruntime.block_on(async move {
            let _r = h1.await;
            let _r2 = h2.await;
        });
    }

    #[test]
    fn should_receive_records_repeatedly(){
        let (uri, _host, _port) = get_vars();
        let status = Arc::new(AtomicBool::new(true));
        let source_status = status.clone();
        let source = RedisSource::new(
            source_status, uri.clone(), "test".to_string(), 2);
        let sema = source.sema.clone();
        let notify_q = source.notification_queue.clone();
        let source_arc = Arc::new(source);
        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(10).build().unwrap();
        let aruntime = Arc::new(runtime);
        let notify = Arc::new(Notify::new());
        let arc_notify = notify.clone();
        let h1 = aruntime.clone().spawn(async move{
            let _r = source_arc.run(arc_notify).await;
        });
        let addr = uri.clone();
        let h2 = aruntime.clone().spawn(async move{
            notify.notified().await;
            let publisher = redis_connection_utilities::create_paired_conn(
                addr).await;
            for _i in 0..3 as usize {
                let records = get_records();
                let json_str = serde_json::to_string(&records).unwrap();
                let _r: Result<RespValue, error::Error> = publisher.send(
                    resp_array!["PUBLISH", "test", json_str.as_str()]).await;
                let waiter = RedisSource::queue_for_next(
                    sema.clone(), notify_q.clone()).await;
                let mut mguard = waiter.lock().await;
                let waiter_result = mguard.receive().await;
                assert!(waiter_result.is_ok());
                let b = waiter_result.unwrap();
                assert_eq!(b.size(), 2);
            }
            status.store(false, Ordering::Relaxed);
        });
        aruntime.block_on(async move {
            let _r = h1.await;
            let _r2 = h2.await;
        });
    }
}
