//! A waiter that notifies the waiting tokio function that records are ready to process.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use tokio::sync::{Notify, oneshot::{Receiver, Sender}, oneshot};
use tokio::sync::oneshot::error::TryRecvError;

use crate::records::batch::Batch;

/// Sender to store elements for the sending end.
pub struct NotificationItem{
    notifier: Arc<Notify>,
    sender: Sender<Batch>
}


impl NotificationItem{

    /// Send the batch
    ///
    /// # Variable
    /// * `batch` - The batch to send
    pub fn send_batch(self, batch: Batch){
        let _r = self.sender.send(batch);
        self.notifier.notify_one();
    }
}


/// Waiter for the receiving end
pub struct BatchWaiter{
    receiver: Receiver<Batch>,
    notifier: Arc<Notify>
}


impl BatchWaiter{

    /// Receive the next batch
    pub async fn receive(&mut self) -> Result<Batch, TryRecvError>{
        self.notifier.notified().await;
        let r = self.receiver.try_recv();
        r
    }


    /// Creates a new notification item
    pub fn new() -> (Self, NotificationItem){
        let (sender, receiver) = oneshot::channel();
        let arc_notify = Arc::new(Notify::new());
        let waiter = BatchWaiter{
            receiver: receiver,
            notifier: arc_notify.clone()
        };
        let notification_item = NotificationItem{
            notifier: arc_notify,
            sender
        };
        (waiter, notification_item)
    }
}



#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::sync::Arc;

    use serde_json::Value;
    use tokio::runtime::{Builder, Runtime};
    use tokio::task::JoinHandle;

    use crate::records::batch::Batch;
    use crate::sources::wait::BatchWaiter;

    fn get_runtime() -> Arc<Runtime>{
        let rt = Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        Arc::new(rt)
    }

    fn receive_batch(rt: Arc<Runtime>, waiter: BatchWaiter, sz: usize) -> JoinHandle<Batch>{
        let mut rwaiter = waiter;
        rt.spawn(async move{
            let b = rwaiter.receive().await;
            assert!(b.is_ok());
            let batch = b.unwrap();
            assert_eq!(batch.size(), sz);
            batch
        })
    }

    #[test]
    pub fn should_work_across_runtime_boundary(){
        let rt = get_runtime();
        let (waiter, notifier) = BatchWaiter::new();
        let handle = receive_batch(rt.clone(), waiter, 0);
        let b = Batch::new(vec![]);
        let s = notifier.sender.send(b);
        assert!(s.is_ok());
        notifier.notifier.notify_one();
        rt.block_on(async move{
           let nb =  handle.await;
            assert!(nb.is_ok());
            assert_eq!(nb.unwrap().size(), 0);
        });
    }

    #[test]
    fn should_receive_records(){
        let rt = get_runtime();
        let (waiter, notifier) = BatchWaiter::new();
        let handle = receive_batch(rt.clone(), waiter, 1);
        let mut record = HashMap::<String, Value>::new();
        record.insert("test".to_string(), Value::Bool(true));
        let mut records = Vec::new();
        records.push(record);
        let b = Batch::new(records);
        let s = notifier.sender.send(b);
        assert!(s.is_ok());
        notifier.notifier.notify_one();
        rt.block_on(async move{
            let nb =  handle.await;
            assert!(nb.is_ok());
            let mut nv = nb.unwrap();
            assert_eq!(nv.size(), 1);
            let r = nv.next();
            assert!(r.is_some());
            let rm = r.unwrap();
            let rv = rm.get("test");
            assert!(rv.is_some());
            assert!(rv.unwrap().as_bool().unwrap());
        });
    }
}
