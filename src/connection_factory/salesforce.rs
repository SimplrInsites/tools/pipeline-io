//! A salesforce data source
//!
//! ---
//! author: Andrew Evans
//! ---


use rustforce::Client;
use tokio::sync::Mutex;
use std::sync::Arc;

/// Salesforce Data Source
#[derive(Clone)]
pub struct SalesForceDataSource{
    user_id: String,
    user_secret: String,
    username: String,
    pwd: String,
    client: Option<Arc<Mutex<Client>>>
}

impl SalesForceDataSource{

    /// Get the salesforce connection
    pub(in crate) fn get_connection(&mut self) -> Arc<Mutex<Client>>{
        let cl_opt = self.client.take();
        let cl_arc = cl_opt.unwrap();
        cl_arc.clone()
    }

    /// Build the connection
    pub async fn build(&mut self){
        let mut client = Client::new(
            self.user_id.clone(), self.user_secret.clone());
        let _r = client.login_with_credential(
            self.username.clone(), self.pwd.clone()).await;
        self.client = Some(Arc::new(Mutex::new(client)));
    }

    /// Create a new data source.
    ///
    /// # Variable
    /// * `user_id` -   The user id
    /// * `user_secret`  - The salesforce secret
    /// * `username` - The username
    /// * `pwd` - Password for salesforce
    pub fn new(user_id: String,
               user_secret: String,
               username: String,
               pwd: String) -> SalesForceDataSource{
        SalesForceDataSource{
            user_id,
            user_secret,
            username,
            pwd,
            client: None
        }
    }
}
