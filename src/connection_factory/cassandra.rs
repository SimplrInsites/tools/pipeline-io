//! A source connection_factory for connecting to data sources/
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use cdrs::authenticators::StaticPasswordAuthenticator;
use cdrs::cluster::{ClusterTcpConfig, NodeTcpConfigBuilder, TcpConnectionPool};
use cdrs::cluster::session::{new as new_session, Session};
use cdrs::load_balancing::RoundRobin;
use tokio::sync::Mutex;

pub type CurrentSession = Session<RoundRobin<TcpConnectionPool<StaticPasswordAuthenticator>>>;


/// ID Columnt Type
#[derive(Clone, Debug)]
pub enum IDType{
    Number,
    DateTime
}


/// The Cassandra Data Source
#[derive(Clone)]
pub struct CassandraDataSource{
    host: String,
    port: u16,
    user: String,
    pwd: String,
    session: Option<Arc<Mutex<CurrentSession>>>
}


impl CassandraDataSource{

    /// Obtain the session as a connection.
    pub async fn get_connection(&mut self) -> Option<Arc<Mutex<CurrentSession>>>{
        if self.session.is_none(){
            self.build();
        }
        let session_opt = self.session.clone();
        session_opt
    }

    /// Build the DataSource
    pub fn build(&mut self){
        let uri = format!("{}:{}", self.host.clone(), self.port.clone().to_string());
        let auth = StaticPasswordAuthenticator::new(
            self.user.clone(), self.pwd.clone());
        let node = NodeTcpConfigBuilder::new(
            uri.as_str(), auth).build();
        let cluster_config = ClusterTcpConfig(vec![node]);
        let session: CurrentSession = new_session(
            &cluster_config, RoundRobin::new()).expect(
            "session should be created");
        self.session = Some(Arc::new(Mutex::new(session)));
    }


    /// Create a new data source
    ///
    /// # Arguments
    /// * `host` - Host string
    /// * `port` - Cassandra port
    /// * `user` - Cassandra user
    /// * `pwd`  - Cassandra password
    pub fn new(host: String, port: u16, user: String, pwd: String) -> CassandraDataSource{
        CassandraDataSource{
            host,
            port,
            user,
            pwd,
            session: None
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::env;

    use cdrs::query::QueryExecutor;

    use crate::connection_factory::cassandra::CassandraDataSource;

    fn get_env_vars() -> (String, u16, String, String){
        let host = env::var("cassandra_host").unwrap();
        let port_string = env::var("cassandra_port").unwrap();
        let user = env::var("cassandra_user").unwrap();
        let pwd = env::var("cassandra_pwd").unwrap();
        let port = u16::from_str_radix(&port_string, 10).unwrap();
        (host, port, user, pwd)
    }

    #[test]
    fn should_create_cassandra_data_source(){
        let (host, port, user, pwd) = get_env_vars();
        let mut ds = CassandraDataSource::new(
            host, port, user, pwd);
        ds.build();
        let rtr = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4).build();
        let rt = rtr.unwrap();
        rt.block_on(async move{
            let conn_opt = ds.get_connection().await;
            assert!(conn_opt.is_some());
            let conn = conn_opt.unwrap();
            let _cguard = conn.lock().await;
        });
    }

    #[test]
    fn should_create_and_drop_namespace(){
        let (host, port, user, pwd) = get_env_vars();
        let mut ds = CassandraDataSource::new(
            host, port, user, pwd);
        ds.build();
        let rtr = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4).build();
        let rt = rtr.unwrap();
        rt.block_on(async move{
            let conn_opt = ds.get_connection().await;
            assert!(conn_opt.is_some());
            let conn = conn_opt.unwrap();
            let cgaurd = conn.lock().await;
            let create_ks: &'static str = "CREATE KEYSPACE IF NOT EXISTS test_ks WITH REPLICATION = { \
                                 'class' : 'SimpleStrategy', 'replication_factor' : 1 };";
            let drop_ks: &'static str = "DROP KEYSPACE IF EXISTS test_ks";
            let mut r = cgaurd.query(create_ks);
            assert!(r.is_ok());
            r = cgaurd.query(drop_ks);
            assert!(r.is_ok());
        });
    }

}