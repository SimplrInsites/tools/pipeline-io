//! Postgresql connection factory
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use tokio::sync::Mutex;
use tokio_postgres::{Client, NoTls};

/// Client PostgreSQL Connection
pub (in crate) struct ClientConnection{
    pub client: Client,
    pub connection_handle: tokio::task::JoinHandle<()>
}


/// PostgreSQL Data Source
#[derive(Clone)]
pub struct PostgreSQLDataSource{
    host: String,
    user: String,
    db: String,
    pwd: String,
    port: u16,
    client: Option<Arc<Mutex<ClientConnection>>>
}


impl PostgreSQLDataSource{

    /// Get whether the client exists
    pub fn is_instantiated(&self) -> bool{
        if self.client.is_some(){
            true
        }else{
            false
        }
    }

    /// Get the client as a connection
    pub (in crate) fn get_connection(&mut self) -> Arc<Mutex<ClientConnection>>{
        let cl_opt = self.client.clone();
        let cl_arc = cl_opt.unwrap();
        cl_arc.clone()
    }

    /// Return whether the connection is alive
    pub async fn is_alive(&mut self) -> bool{
        let arc_client = self.client.clone().unwrap();
        let mgaurd = arc_client.lock().await;
        if mgaurd.client.is_closed(){
            false
        }else{
            true
        }
    }

    /// Check that the existing client connection is alive. Recreate it otherwise.
    pub async fn check_conn(&mut self){
        let arc_client = self.client.clone().unwrap();
        let mgaurd = arc_client.lock().await;
        if mgaurd.client.is_closed(){
            drop(mgaurd);
            self.build().await
        }
    }

    /// Create a thread safe connection and set it in the structure
    pub async fn build(&mut self) {
        let uri = format!(
            "host={} port={} dbname={} user={} password={}",
            self.host.clone(),
            self.port.clone().to_string(),
            self.db,
            self.user.clone(),
            self.pwd.clone());
        let (client, connection) =
            tokio_postgres::connect(&uri, NoTls).await.unwrap();
        let handle = tokio::spawn(async move{
            let _r = connection.await;
        });
        let cl_conn = ClientConnection{
            client,
            connection_handle: handle
        };
        let arc_cl_conn =  Arc::new(Mutex::new(cl_conn));
        if self.client.is_some(){
            let clopt = self.client.take();
            self.client = Some(arc_cl_conn);
            drop(clopt);
        }else{
            self.client = Some(arc_cl_conn);
        }
    }

    /// Create a new PostgreSQL source
    pub fn new(
        host: String, port: u16, db: String, username: String, password: String) -> PostgreSQLDataSource{
        PostgreSQLDataSource{
            host,
            user: username,
            db,
            pwd: password,
            port,
            client: None
        }
    }
}


#[cfg(test)]
pub mod tests{
    use crate::connection_factory::postgresql::PostgreSQLDataSource;

    fn get_vars() -> (String, u16, String, String, String){
        let host = std::env::var("postgresql_host").unwrap();
        let port_string = std::env::var("postgresql_port").unwrap();
        let db = std::env::var("postgresql_db").unwrap();
        let user = std::env::var("postgresql_user").unwrap();
        let pwd = std::env::var("postgresql_pwd").unwrap();
        let port = u16::from_str_radix(&port_string, 10).unwrap();
        (host, port, db, user, pwd)
    }

    #[test]
    fn should_create_connection(){
        let rtr = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4).build();
        let rt = rtr.unwrap();
        let (host, port, db, user, pwd) = get_vars();
        rt.block_on(async move {
            let mut factory = PostgreSQLDataSource::new(host, port, db, user, pwd);
            factory.build().await;
        });
    }

    #[test]
    fn should_query(){
        let rtr = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4).build();
        let rt = rtr.unwrap();
        let (host, port, db, user, pwd) = get_vars();
        let mut factory = PostgreSQLDataSource::new(host, port, db, user, pwd);
        rt.block_on(async move {
            factory.build().await;
            let arc_fact = factory.get_connection();
            let agaurd = arc_fact.lock().await;
            let r = agaurd.client.query("SELECT $1::TEXT", &[&"hello world"]).await;
            assert!(r.is_ok());
            let v = r.unwrap();
            assert!(v.len() == 1);
            let row = v.get(0).unwrap();
            let vstr: &str = row.get(0);
            assert_eq!(vstr, "hello world");
        });
    }

    #[test]
    fn should_query_empty(){
        let rtr = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4).build();
        let rt = rtr.unwrap();
        let (host, port, db, user, pwd) = get_vars();
        let mut factory = PostgreSQLDataSource::new(host, port, db, user, pwd);
        rt.block_on(async move {
            factory.build().await;
            let arc_fact = factory.get_connection();
            let agaurd = arc_fact.lock().await;
            let r = agaurd.client.query(
                "SELECT 'hello world'", &[]).await;
            assert!(r.is_ok());
            let v = r.unwrap();
            assert!(v.len() == 1);
            let row = v.get(0).unwrap();
            let vstr: &str = row.get(0);
            assert_eq!(vstr, "hello world");
        });
    }
}
