//! Redis connection factory
//!
//! ---
//! author: Andrew Evans
//! ---

use redis_async::{client, error};
use redis_async::client::{PairedConnection, PubsubConnection};

/// Blocking get on a pubsub conn. Sets the conn and returns success or error.
///
/// # Arguments
/// * `addr` - Address to the backend
pub async fn create_pubsub_conn(addr: String) -> Result<PubsubConnection, error::Error>{
    let socket_addr = addr.parse().unwrap();
    let conn = client::pubsub_connect(&socket_addr).await;
    conn
}


/// Get a connection for publishing
///
/// # Variables
/// * `addr`    -   The address
pub async fn create_paired_conn(addr: String) -> PairedConnection{
    let url = addr.parse().unwrap();
    let paired_result = client::paired_connect(&url).await;
    debug_assert_eq!(paired_result.is_ok(), true);
    paired_result.ok().unwrap()
}


#[cfg(test)]
pub mod tests{
    use crate::connection_factory::redis_connection_utilities::{create_pubsub_conn, create_paired_conn};

    /// Get test environment variables
    fn get_vars() -> (String, String, i64){
        let host = std::env::var("REDISHOST").unwrap();
        let port_string = std::env::var("REDISPORT").unwrap();
        let redis_uri = format!("{}:{}", host.clone(), port_string);
        (redis_uri, host, i64::from_str_radix(port_string.as_str(), 10).unwrap())
    }

    /// Test that a pubsub conn can be created.
    #[test]
    fn should_create_pub_sub_conn(){
        let (url, _host, _port) = get_vars();
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build()
            .unwrap();
        rt.block_on(async move {
            let psconn = create_pubsub_conn(url).await;
            assert!(psconn.is_ok());
        });
    }

    /// Test that a paired connection works.
    #[test]
    fn should_create_paired_conn(){
        let (url, _host, _port) = get_vars();
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build()
            .unwrap();
        rt.block_on(async move {
            let _psconn = create_paired_conn(url).await;
        });
    }
}