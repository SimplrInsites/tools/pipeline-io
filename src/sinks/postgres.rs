//! The PostgreSQL sink
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use chrono::Utc;
use tokio::sync::mpsc::{Receiver, Sender};
use tokio::time::Duration;
use tokio::time::error::Elapsed;

use crate::connection_factory::postgresql::PostgreSQLDataSource;
use crate::records::batch::Batch;
use crate::records::cache::RecordCache;
use crate::sinks::middleware::postgres::PostHandler;

/// The Postgres Sink
pub struct PostgreSink{
    status: Arc<AtomicBool>,
    data_source: PostgreSQLDataSource,
    signaler: Arc<Sender<u8>>,
    sink_index: u8,
    batch_size: usize
}


impl PostgreSink{

    /// Convert column names to query parts.
    ///
    /// # Variables
    /// * `keys` - The keys
    pub fn convert_columns_to_names(keys: Vec<String>) -> String{
        let mut val_part = "(".to_string();
        let mut col_part = "(".to_string();
        let mut idx = 0;
        for key in keys{
            let sidx = idx.to_string();
            if idx > 0{
                val_part = format!("{},${}", val_part, sidx);
                col_part = format!("{}, {}", col_part, key);
            }else{
                val_part = format!("{}{}", val_part, sidx);
                col_part = format!("{}{}", col_part, key)
            }
            idx += 1;
        }
        format!("{}) {})", col_part, val_part)
    }

    /// Build the postgresql query
    ///
    /// # Variables
    /// * `table` - The table name
    pub fn create_query(table: String, keys: Vec<String>) -> String{
        let s = format!("INSERT INTO {}", table);
        let val_query = PostgreSink::convert_columns_to_names(keys);
       format!("{} {}", s, val_query)
    }

    /// Run the sink
    ///
    /// # Variables
    /// * `receiver` -  Receiver to get records
    pub async fn run(&self, handler: PostHandler, receiver: Receiver<Batch>){
        let mut handler = handler;
        let signaler = self.signaler.clone();
        let sink_index = self.sink_index.clone();
        let mut rcvr = receiver;
        let mut client = self.data_source.clone();
        let mut cache = RecordCache::new();
        let status = self.status.clone();
        let mut last_insert = Utc::now();
        while status.load(Ordering::Relaxed){
            let rf = rcvr.recv();
            let to: Result<Option<Batch>, Elapsed> = tokio::time::timeout(
                Duration::new(5, 0), rf).await;
            if to.is_ok() {
                let bopt = to.unwrap();
                if bopt.is_some() {
                    let b = bopt.unwrap();
                    let records = b.get_records();
                    cache.add_records(records);
                    let nw = Utc::now();
                    let dur = last_insert.signed_duration_since(nw);
                    let time_since = dur.num_seconds() + (dur.num_minutes() * 60);
                    let mut insert = false;
                    if time_since > 120{
                        insert = true;
                    }
                    while cache.size() >= self.batch_size || insert {
                        let batch = cache.get_n_records(self.batch_size);
                        let _f = handler.run(batch, &mut client).await;
                        last_insert = Utc::now();
                        insert = false;
                    }
                }
            }
            let _r = signaler.send(sink_index).await;
        }
        while cache.size() > 0{
            let batch = cache.get_n_records(self.batch_size);
            handler.run(batch, &mut client).await;
        }
    }

    /// Create a new Postgres Sink
    pub fn new(
        status: Arc<AtomicBool>,
        data_source: PostgreSQLDataSource,
        signaler: Arc<Sender<u8>>,
        sink_index: u8,
        batch_size: usize) -> PostgreSink{
        PostgreSink{
            status,
            data_source,
            signaler,
            sink_index,
            batch_size,
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};

    use serde_json::{Number, Value};
    use tokio::sync::mpsc::channel;

    use crate::connection_factory::postgresql::PostgreSQLDataSource;
    use crate::records::batch::Batch;
    use crate::records::conversion_utils;
    use crate::sinks::middleware::postgres::PostHandler;
    use crate::sinks::postgres::PostgreSink;

    fn get_vars() -> (String, u16, String, String, String){
        let host = std::env::var("postgresql_host").unwrap();
        let port_string = std::env::var("postgresql_port").unwrap();
        let db = std::env::var("postgresql_db").unwrap();
        let user = std::env::var("postgresql_user").unwrap();
        let pwd = std::env::var("postgresql_pwd").unwrap();
        let port = u16::from_str_radix(&port_string, 10).unwrap();
        (host, port, db, user, pwd)
    }

    async fn truncate_table(ds: &mut PostgreSQLDataSource){
        let conn = ds.get_connection();
        let mguard = conn.lock().await;
        let q = mguard.client.prepare(
            "TRUNCATE TABLE test.dc_test_table").await;
        let stmt = q.unwrap();
        let r = mguard.client.execute(&stmt, &[]).await;
        assert!(r.is_ok());
    }

    async fn insert_records(b: Batch, ds: &mut PostgreSQLDataSource) -> (){
        let records = b.get_records();
        let conn = ds.get_connection();
        let mguard = conn.lock().await;
        let mut q = "INSERT INTO test.dc_test_table (test_str, test_num) VALUES".to_string();
        let mut start: i32 = 0;
        for record in records {
            let tstr = conversion_utils::get_value_as_string(
                &record,"test_string").unwrap();
            let tnum = conversion_utils::get_value_as_i32(
                &record, "test_num").unwrap();
            if start > 0 {
                q = format!("{}, ('{}',{})", q, tstr, tnum.to_string());
            }else{
                q = format!("{} ('{}',{})", q, tstr, tnum.to_string());
            }
            start += 1
        }
        let f = mguard.client.simple_query(q.as_str()).await;
        let _r = f.unwrap();
    }


    #[test]
    fn should_post_records(){
        let (host, port, db, user, pwd) = get_vars();
        let mut ds = PostgreSQLDataSource::new(
            host, port, db, user, pwd);
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        rt.block_on(async move{
            let _r = ds.build().await;
            let mut records = vec![];
            let mut mp = HashMap::new();
            mp.insert("test_string".to_string(), Value::String("test".to_string()));
            mp.insert("test_num".to_string(), Value::Number(Number::from(100)));
            records.push(mp);
            let b = Batch::new(records);
            let _r = insert_records(b, &mut ds).await;
            let _r = truncate_table(&mut ds).await;
        });
    }

    #[test]
    fn should_create_sink(){
        let (host, port, db, user, pwd) = get_vars();
        let mut ds = PostgreSQLDataSource::new(
            host, port, db, user, pwd);
        let status = Arc::new(AtomicBool::new(true));
        let (sender, _receiver) = channel::<u8>(100);
        let (_batch_sender, batch_receiver) = channel::<Batch>(1000);
        let handler: PostHandler = PostHandler {
            f:Box::new( | b, s|{
                Box::pin(async move {
                    insert_records(b, s).await;
                })
            })
        };
        let arc_status = status.clone();
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        let h1 = rt.spawn(async move{
            let _r = ds.build().await;
            let sink = PostgreSink::new(
                status,
                ds,
                Arc::new(sender),
                0,
                100);
            let _r = sink.run(handler, batch_receiver).await;
        });
        rt.block_on(async move{
            arc_status.store(false, Ordering::Relaxed);
            let _r = h1.await;
        });
    }

    #[test]
    fn should_post_batches(){
        let (host, port, db, user, pwd) = get_vars();
        let mut ds = PostgreSQLDataSource::new(
            host, port, db, user, pwd);
        let status = Arc::new(AtomicBool::new(true));
        let (sender, mut receiver) = channel::<u8>(100);
        let (batch_sender, batch_receiver) = channel::<Batch>(1000);
        let handler: PostHandler = PostHandler {
            f:Box::new( | b, s|{
                Box::pin(async move {
                    insert_records(b, s).await;
                })
            })
        };
        let mut ds2 = ds.clone();
        let arc_status = status.clone();
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        let h1 = rt.spawn(async move{
            ds.build().await;
            let sink = PostgreSink::new(
                arc_status,
                ds,
                Arc::new(sender),
                0,
                100);
            let _r = sink.run(handler, batch_receiver).await;
        });
        let h2 = rt.spawn(async move{
            for _j in 0..3 as usize {
                let mut records = vec![];
                for _i in 0..1000 as usize {
                    let mut mp = HashMap::new();
                    mp.insert("test_string".to_string(), Value::String("test".to_string()));
                    mp.insert("test_num".to_string(), Value::Number(Number::from(100)));
                    records.push(mp);
                }
                let b = Batch::new(records);
                let r = batch_sender.send(b).await;
                assert!(r.is_ok());
                let signal_r = receiver.recv().await;
                assert!(signal_r.is_some());
                let idx = signal_r.unwrap();
                assert_eq!(idx, 0);
            }
            ds2.build().await;
            let _r = truncate_table(&mut ds2);
            status.store(false, Ordering::Relaxed);
        });
        rt.block_on(async move{
            let _r = h2.await;
            let _r = h1.await;
        });
    }
}