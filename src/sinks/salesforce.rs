//! Salesforce sink. There doesn't appear to be a supported
//! tool for batch inserts yet.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use tokio::sync::mpsc::{Receiver, Sender};
use tokio::task::JoinHandle;
use tokio::time::Duration;
use tokio::time::error::Elapsed;

use crate::connection_factory::salesforce::SalesForceDataSource;
use crate::records::batch::Batch;
use crate::records::cache::RecordCache;
use crate::sinks::middleware::salesforce::PostHandler;
use crate::sinks::utils::map_records;

pub struct SalesForceSink{
    status: Arc<AtomicBool>,
    data_source: SalesForceDataSource,
    column_map: Arc<HashMap<String, String>>,
    signaler: Arc<Sender<u8>>,
    sink_index: u8,
    post_handler: Arc<PostHandler>
}


impl SalesForceSink{

    pub async fn run(&mut self, receiver: Receiver<Batch>){
        let mut rcvr = receiver;
        let column_map = self.column_map.clone();
        let sink_idx = self.sink_index.clone();
        let signaler = self.signaler.clone();
        let mut ds = self.data_source.clone();
        let client = ds.get_connection();
        let handler = self.post_handler.clone();
        let mut cache = RecordCache::new();
        let status = self.status.clone();
        let f = handler.f;
        while status.load(Ordering::Relaxed){
            let rf = rcvr.recv();
            let to: Result<Option<Batch>, Elapsed> = tokio::time::timeout(
                Duration::new(5, 0), rf).await;
            if to.is_ok() {
                let bopt = to.unwrap();
                if bopt.is_some() {
                    let mut b = bopt.unwrap();
                    b = map_records(column_map.clone(), b);
                    let h: JoinHandle<()> = f(b, client.clone());
                    let _r = h.await;
                    cache.update_records(vec![]);
                }
            }
            let _r = signaler.send(sink_idx.clone());
        }
    }

    /// Create a new sink
    pub fn new(status: Arc<AtomicBool>,
               data_source: SalesForceDataSource,
               column_map: Arc<HashMap<String, String>>,
               signaler: Arc<Sender<u8>>,
               sink_index: u8,
               post_handler: Arc<PostHandler>) -> SalesForceSink{
        SalesForceSink{
            status,
            data_source,
            column_map,
            signaler,
            sink_index,
            post_handler
        }
    }

}
