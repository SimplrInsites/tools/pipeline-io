//! Salesforce middleware
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use rustforce::Client;
use tokio::sync::Mutex;
use tokio::task::JoinHandle;

use crate::records::batch::Batch;

/// Handles posts for salesforce
pub struct PostHandler{
    pub f: fn(batch: Batch, Arc<Mutex<Client>>) -> JoinHandle<()>
}
