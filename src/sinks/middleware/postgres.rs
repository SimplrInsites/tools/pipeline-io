//! Postgres handler
//!
//! ---
//! author: Andrew Evans
//! ---

use futures::future::BoxFuture;

use crate::connection_factory::postgresql::PostgreSQLDataSource;
use crate::records::batch::Batch;

/// Contains the post handler
pub struct PostHandler {
    pub f: Box<dyn for<'a> FnMut(Batch, &'a mut PostgreSQLDataSource) -> BoxFuture<'a, ()> + Send>
}

impl PostHandler {

    pub async fn run(&mut self, b: Batch, ds: &mut PostgreSQLDataSource) {
        (self.f)(b, ds).await;
    }
}


#[cfg(test)]
pub mod tests{
    use crate::connection_factory::postgresql::PostgreSQLDataSource;
    use crate::records::batch::Batch;
    use crate::sinks::middleware::postgres::PostHandler;

    async fn test(_b: Batch, _ds: &mut PostgreSQLDataSource){
        println!("Working");
    }

    #[test]
    fn should_run_asynchronously(){
        let b = Batch::new(vec![]);
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        rt.block_on(async move {
            let mut ds = PostgreSQLDataSource::new(
                "".to_string(),
                5432,
                "test".to_string(),
                "test".to_string(),
                "test".to_string());
             let mut handler = PostHandler{
                 f: Box::new(|b, s| {
                        Box::pin(async move {
                             test(b, s).await;
                        })
                    })
             };
            handler.run(b.clone(), &mut ds).await;
            handler.run(b, &mut ds).await;
        });
    }
}