//! Cassandra middleware
//!
//! ---
//! author: Andrew Evans
//! ---


use std::sync::Arc;

use tokio::sync::Mutex;
use tokio::task::JoinHandle;

use crate::connection_factory::cassandra::CurrentSession;
use crate::records::batch::Batch;

#[derive(Clone)]
pub struct PostHandler{
    pub f: fn(Batch, Arc<Mutex<CurrentSession>>) -> JoinHandle<()>
}
