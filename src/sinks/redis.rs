//! A redis sink for pushing records.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use redis_async::{error, resp_array};
use redis_async::resp::RespValue;
use tokio::sync::mpsc::{Receiver, Sender};
use tokio::time::Duration;
use tokio::time::error::Elapsed;

use crate::connection_factory::redis_connection_utilities;
use crate::records::batch::Batch;
use crate::sinks::utils::map_records;

/// A redis sink
pub struct RedisSink{
    status: Arc<AtomicBool>,
    route: String,
    url: String,
    signaler: Arc<Sender<u8>>,
    sink_index: u8,
    column_map: Arc<HashMap<String, String>>
}

impl RedisSink{

    /// Run the sink
    ///
    /// # Variable
    /// * `receiver`    -   The sink receiver
    pub async fn run(&mut self, receiver: Receiver<Batch>) {
        let sink_index = self.sink_index.clone();
        let route = self.route.clone();
        let mut rcvr = receiver;
        let signaler = self.signaler.clone();
        let cmap = self.column_map.clone();
        let addr = self.url.clone();
        let conn = redis_connection_utilities::create_paired_conn(addr).await;
        let arc = self.status.clone();
        while arc.load(Ordering::Relaxed) {
            let rf = rcvr.recv();
            let record_r: Result<Option<Batch>, Elapsed> = tokio::time::timeout(
                Duration::new(0, 750), rf).await;
            if record_r.is_ok() {
                let bopt = record_r.unwrap();
                if bopt.is_some() {
                    let mut b = bopt.unwrap();
                    b = map_records(cmap.clone(), b);
                    let jsonr = b.to_json();
                    if jsonr.is_ok() {
                        let json_str = jsonr.unwrap();
                        let sval: Result<RespValue, error::Error> = conn.send(
                            resp_array!["PUBLISH", route.as_str(), json_str.as_str()]).await;
                        if sval.is_err(){
                            let e = sval.err().unwrap();
                            println!("{}", e.to_string());
                        }
                    }else{
                        println!("Failed to map to json");
                    }
                }
                let _r = signaler.send(sink_index).await;
            }
        }
    }

    /// Create a new Redis Sink
    ///
    /// # Variables
    /// * `status` - Status of the applicationb
    /// * `route` -  The route
    /// * `url`   -  Connection URL for Redis
    /// * `column_map` -   Column mapping
    pub fn new(
        status: Arc<AtomicBool>,
        route: String,
        url: String,
        signaler: Arc<Sender<u8>>,
        sink_index: u8,
        column_map: Arc<HashMap<String, String>>) -> RedisSink{
        RedisSink{
            status,
            route,
            url,
            signaler,
            sink_index,
            column_map
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::sync::atomic::{AtomicBool, Ordering};
    use std::sync::Arc;
    use crate::sinks::redis::RedisSink;
    use tokio::sync::mpsc::channel;
    use crate::records::batch::Batch;
    use std::collections::HashMap;
    use serde_json::{Number, Value};

    /// Get test environment variables
    fn get_vars() -> (String, String, i64){
        let host = std::env::var("REDISHOST").unwrap();
        let port_string = std::env::var("REDISPORT").unwrap();
        let redis_uri = format!("{}:{}", host.clone(), port_string);
        (redis_uri, host, i64::from_str_radix(port_string.as_str(), 10).unwrap())
    }

    #[test]
    fn should_setup_sink(){
        let (_batch_sender, batch_receiver) = channel::<Batch>(1000);
        let (sender, _receiver) = channel::<u8>(1000);
        let (uri, _host, _port) = get_vars();
        let status = Arc::new(AtomicBool::new(true));
        let mut sink = RedisSink::new(
            status.clone(),
            "test".to_string(),
            uri.clone(),
            Arc::new(sender),
        0,
        Arc::new(HashMap::new()));
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        rt.block_on(async move{
            let r = sink.run(batch_receiver);
            status.store(false, Ordering::Relaxed);
            let _r = r.await;
        });
    }

    #[test]
    fn should_process_multiple_records(){
        let (uri, _host, _port) = get_vars();
        let (batch_sender, batch_receiver) = channel::<Batch>(1000);
        let (sender, mut receiver) = channel::<u8>(1000);
        let status = Arc::new(AtomicBool::new(true));
        let mut sink = RedisSink::new(
            status.clone(),
            "test".to_string(),
            uri.clone(),
            Arc::new(sender),
            0,
            Arc::new(HashMap::new()));
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        let h = rt.spawn(async move{
            sink.run(batch_receiver).await;
        });
        let h2 = rt.spawn(async move{
            let mut records = vec![];
            for _j in 0..10 as usize {
                for i in 0..1000 as usize {
                    let mut record = HashMap::new();
                    record.insert("test_id".to_string(), Value::Number(Number::from(i)));
                    record.insert("test_str".to_string(), Value::String("test".to_string()));
                    record.insert("test_num".to_string(), Value::Number(Number::from(i * 3)));
                    records.push(record);
                }
            }
            let b = Batch::new(records);
            let r = batch_sender.send(b).await;
            assert!(r.is_ok());
            let m = receiver.recv().await;
            assert!(m.is_some());
            status.store(false, Ordering::Relaxed);
        });
        rt.block_on(async move{
            let _r = h2.await;
            let _r = h.await;
        });
    }
}
