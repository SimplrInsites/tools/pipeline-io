//! Common sink utilities
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;
use std::sync::Arc;

use crate::records::batch::Batch;

/// Map a batch to the new format.
///
/// # Variables
/// * `column_map`  -   Mapping of records
/// * `record`  -   The record to map
pub fn map_records(column_map: Arc<HashMap<String, String>>, batch: Batch) -> Batch{
    if column_map.len() > 0{
        let mut new_records = vec![];
        let mut b = batch;
        let mut record_opt = b.next();
        while record_opt.is_some() {
            let record = record_opt.unwrap();
            let mut new_record = HashMap::new();
            for (k, v) in record {
                let new_k = column_map.get(k.as_str());
                if new_k.is_some() {
                    new_record.insert(new_k.unwrap().clone(), v.clone());
                } else {
                    new_record.insert(k.clone(), v.clone());
                }
            }
            new_records.push(new_record);
            record_opt = b.next();
        }
        Batch::new(new_records)
    }else{
        batch
    }
}