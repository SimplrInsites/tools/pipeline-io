//! A sink for pushing records to Cassandra.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use chrono::Utc;
use tokio::sync::mpsc::Receiver;
use tokio::sync::mpsc::Sender;
use tokio::task::JoinHandle;
use tokio::time::Duration;

use crate::connection_factory::cassandra::CassandraDataSource;
use crate::records::batch::Batch;
use crate::records::cache::RecordCache;
use crate::sinks::middleware::cassandra::PostHandler;
use crate::sinks::utils::map_records;

/// A sink for pushing records to Cassandra
pub struct CassandraSink{
    status: Arc<AtomicBool>,
    data_source: CassandraDataSource,
    column_map: Arc<HashMap<String, String>>,
    signaler: Arc<Sender<u8>>,
    sink_index: u8,
    batch_size: usize,
    post_handler: Arc<PostHandler>
}


impl CassandraSink{

    /// Run the sink
    ///
    /// # Variables
    /// * `receiver` - Receiver for incoming records
    pub async fn run(&mut self, receiver: Receiver<Batch>){
        let column_map = self.column_map.clone();
        let mut rcvr = receiver;
        let batch_size = self.batch_size.clone();
        let handler = self.post_handler.clone();
        let mut cache = RecordCache::new();
        let status = self.status.clone();
        let mut ds = self.data_source.clone();
        let sess_opt = ds.get_connection().await;
        let sess = sess_opt.unwrap();
        let signaler = self.signaler.clone();
        let sink_index = self.sink_index.clone();
        let mut last_insert = Utc::now();
        let f = handler.f;
        while status.load(Ordering::Relaxed){
            let rf = rcvr.recv();
            let batch_r = tokio::time::timeout(
                Duration::new(5, 0), rf).await;
            if batch_r.is_ok(){
                let batch_opt = batch_r.unwrap();
                if batch_opt.is_some(){
                    let mut b = batch_opt.unwrap();
                    b = map_records(column_map.clone(), b);
                    let records = b.get_records();
                    cache.add_records(records);
                    let nw = Utc::now();
                    let dur = last_insert.signed_duration_since(nw);
                    let time_since = dur.num_seconds() + (dur.num_minutes() * 60);
                    let mut insert = false;
                    if time_since > 120{
                        insert = true;
                    }
                    if cache.size() >= batch_size || insert {
                        let records = cache.records.clone();
                        let b = Batch::new(records);
                        let h: JoinHandle<()> = f(b.clone(), sess.clone());
                        let _r = h.await;
                        cache.update_records(vec![]);
                        last_insert = Utc::now();
                    }
                    let _r = signaler.send(sink_index.clone()).await;
                }
            }
        }
        if cache.records.len() > 0{
            let records = cache.records;
            let b = Batch::new(records);
            let h: JoinHandle<()> = f(b.clone(), sess.clone());
            let _r = h.await;
        }
    }

    /// Create a new cassandra sink
    ///
    /// # Variables
    /// * `status` - Application status
    /// * `data_source` - The Cassandra Data Source
    /// * `column_map` - The column map
    /// * `column_map` - Maps record columns to record columns
    /// * `signaler` - For signaling the handler
    /// * `sink_index` - Index id of the sink
    /// * `batch_size` - Batch size to post when reached
    /// * `post_hander` - Handles database posts
    pub fn new(status: Arc<AtomicBool>,
               data_source: CassandraDataSource,
               column_map: Arc<HashMap<String, String>>,
               signaler: Arc<Sender<u8>>,
               sink_index: u8,
               batch_size: usize,
               post_handler: Arc<PostHandler>) -> CassandraSink{
        CassandraSink{
            status,
            data_source,
            column_map,
            signaler,
            sink_index,
            batch_size,
            post_handler
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::env;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};

    use cdrs::query::{BatchExecutor, BatchQueryBuilder, QueryValues};
    use tokio::sync::mpsc::channel;
    use tokio::sync::Mutex;
    use tokio::task::JoinHandle;

    use crate::connection_factory::cassandra::{CassandraDataSource, CurrentSession};
    use crate::records::batch::Batch;
    use crate::records::conversion_utils;
    use crate::sinks::cassandra::CassandraSink;
    use crate::sinks::middleware::cassandra::PostHandler;
    use chrono::Utc;
    use serde_json::{Value, Number};
    use tokio::time::Duration;

    /// Get the environment variables.
    fn get_env_vars() -> (String, u16, String, String){
        let host = env::var("cassandra_host").unwrap();
        let port_string = env::var("cassandra_port").unwrap();
        let user = env::var("cassandra_user").unwrap();
        let pwd = env::var("cassandra_pwd").unwrap();
        let port = u16::from_str_radix(&port_string, 10).unwrap();
        (host, port, user, pwd)
    }

    /// Post records to the database
    fn post_records(b: Batch, ds: Arc<Mutex<CurrentSession>>) -> JoinHandle<()>{
        tokio::spawn(async move {
            let records = b.get_records();
            let mguard = ds.lock().await;
            let mut queries = BatchQueryBuilder::new();
            for record in records {
                let test_id = conversion_utils::get_value_as_i64(
                    &record, "test_id").unwrap();
                let test_date = conversion_utils::get_naive_dt_value_as_cassandra_time_str(
                    &record, "test_date").unwrap();
                let test_str = conversion_utils::get_value_as_string(
                    &record, "test_str").unwrap();
                let test_num = conversion_utils::get_value_as_i32(
                    &record, "test_num").unwrap();
                let fq = format!(
                    "INSERT INTO test.test_table (test_id, test_date, test_str, test_num) \
                    VALUES({}, '{}', '{}' ,{})",
                    test_id.to_string(),
                    test_date,
                    test_str,
                    test_num.to_string());
                queries = queries.add_query(fq, QueryValues::SimpleValues(vec![]));
            }
            let qbatch = queries.finalize().unwrap();
            let r = mguard.batch_with_params_tw(qbatch, true, true);
            if r.is_err() {
                r.unwrap();
            }else{
                let warnings = r.ok().unwrap();
                for warning in warnings.warnings{
                    println!("{}", warning);
                }
            }
        })
    }

    #[test]
    fn test_post_records(){
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        let (host, port, user, pwd) = get_env_vars();
        rt.block_on(async move {
            let mut source = CassandraDataSource::new(host, port, user, pwd);
            source.build();
            let dt = Utc::now();
            let naive_dt = dt.naive_utc();
            let naive_dt_str = naive_dt.to_string();
            let mut records = vec![];
            let mut record = HashMap::new();
            record.insert("test_id".to_string(), Value::Number(Number::from(200)));
            record.insert("test_date".to_string(), Value::String(naive_dt_str.clone()));
            record.insert("test_str".to_string(), Value::String("test2".to_string()));
            record.insert("test_num".to_string(), Value::Number(Number::from(3)));
            records.push(record);
            let mut record2 = HashMap::new();
            record2.insert("test_id".to_string(), Value::Number(Number::from(100)));
            record2.insert("test_date".to_string(), Value::String(naive_dt_str));
            record2.insert("test_str".to_string(), Value::String("test".to_string()));
            record2.insert("test_num".to_string(), Value::Number(Number::from(2)));
            records.push(record2);
            let b = Batch::new(records);
            let session = source.get_connection().await;
            let r = post_records(b, session.unwrap()).await;
            assert!(r.is_ok());
        });
    }

    #[test]
    fn should_create_sink(){
        let handler = PostHandler{
            f: post_records
        };
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        let arc_handler = Arc::new(handler);
        let (sender, _receiver) = channel::<u8>(1000);
        let (_batch_sender, batch_receiver) = channel::<Batch>(1000);
        let arc_sender = Arc::new(sender);
        let (host, port, user, pwd) = get_env_vars();
        let status = Arc::new(AtomicBool::new(true));
        let mut source = CassandraDataSource::new(host, port, user, pwd);
        source.build();
        let column_map = Arc::new(HashMap::new());
        let mut sink = CassandraSink::new(
            status.clone(),
            source,
            column_map,
            arc_sender,
            0,
            1000,
            arc_handler);
        let h1 = rt.spawn(async move {
            let _r = sink.run(batch_receiver).await;
        });
        rt.block_on(async move{
           status.store(false, Ordering::Relaxed);
            let _r = h1.await;
        });
    }

    #[test]
    fn should_insert_records(){
        let handler = PostHandler{
            f: post_records
        };
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build().unwrap();
        let arc_handler = Arc::new(handler);
        let (sender, mut receiver) = channel::<u8>(1000);
        let (batch_sender, batch_receiver) = channel::<Batch>(1000);
        let arc_sender = Arc::new(sender);
        let (host, port, user, pwd) = get_env_vars();
        let status = Arc::new(AtomicBool::new(true));
        let mut source = CassandraDataSource::new(host, port, user, pwd);
        source.build();
        let column_map = Arc::new(HashMap::new());
        let mut sink = CassandraSink::new(
            status.clone(),
            source,
            column_map,
            arc_sender,
            0,
            1000,
            arc_handler);
        let h1 = rt.spawn(async move {
            let _r = sink.run(batch_receiver).await;
        });
        let h2 = rt.spawn(async move{
            for _j in 0..4 {
                let mut records = vec![];
                for i in 0..1000 {
                    let dt = Utc::now();
                    let naive_dt = dt.naive_utc();
                    let naive_dt_str = naive_dt.to_string();
                    let mut record = HashMap::new();
                    record.insert("test_id".to_string(), Value::Number(Number::from(i)));
                    record.insert("test_date".to_string(), Value::String(naive_dt_str.clone()));
                    record.insert("test_str".to_string(), Value::String("test".to_string()));
                    record.insert("test_num".to_string(), Value::Number(Number::from(i * 3)));
                    records.push(record);
                }
                let b = Batch::new(records);
                let batch_result = batch_sender.send(b).await;
                assert!(batch_result.is_ok());
                let idxrf = receiver.recv();
                let idxr = tokio::time::timeout(
                    Duration::new(10, 0), idxrf).await;
                assert!(idxr.is_ok());
            }
            status.store(false, Ordering::Relaxed);
        });
        rt.block_on(async move{
            let _r = h2.await;
            let _r = h1.await;
        });
    }
}