//! Utilities for a query
//!
//! ---
//! author: Andrew Evans
//! ---


/// Convert columns to string
///
/// # Variables
/// * `columns` - The columns
pub fn columns_to_string(columns: Vec<String>) -> String{
    if columns.len() > 0{
        let mut column_str = "".to_string();
        for column in columns.clone(){
            if column_str.len() == 0{
                column_str = column;
            }else{
                column_str = format!("{}, {}", column_str, column);
            }
        }
        column_str
    }else{
        "*".to_string()
    }
}


#[cfg(test)]
pub mod tests{
    use crate::query::utils::columns_to_string;

    #[test]
    fn should_return_star_on_empty_columns(){
        let columns = Vec::<String>::new();
        let cstr = columns_to_string(columns);
        assert_eq!(cstr, "*");
    }

    #[test]
    fn should_map_columns_to_names(){
        let columns = vec!["test1".to_string(), "test2".to_string()];
        let cstr = columns_to_string(columns);
        assert_eq!(cstr, "test1, test2");
    }
}
