//! Batch of records.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{HashMap, HashSet};

use serde_json::Value;
use serde_json::error::Result;


/// A batch of records
///
/// # Variables
/// * `records` - Records in the batch
#[derive(Clone, Debug)]
pub struct Batch{
    records: Vec<HashMap<String, Value>>
}

impl Batch{

    /// Get all current records without draining the vector
    pub fn get_records(&self) -> Vec<HashMap<String, Value>>{
        self.records.clone()
    }

    /// Convert the existing records to a json string
    pub fn to_json(&self) -> Result<String>{
        serde_json::to_string(&self.records)
    }

    /// Get the number of records left in the batch
    pub fn size(&self)-> usize{
        self.records.len()
    }

    /// Get the next records or None if they do not exist
    pub fn next(&mut self) -> Option<HashMap<String, Value>>{
        self.records.pop()
    }

    /// Get keys for the record through the batch
    ///
    /// # Variables
    /// * `scan` - Whether to scan through the records
    pub fn get_keys(&self, scan: bool) -> Vec<String>{
        let mut keyvec = Vec::new();
        let mut keyset = HashSet::new();
        if self.records.len() > 0 {
            let mut idx = 0;
            let record = self.records.get(0).unwrap();
            for (k, _v) in record {
                keyset.insert(k);
            }
            if scan {
                idx += 1;
                for i in idx..self.records.len(){
                    let record = self.records.get(i).unwrap();
                    for (k, _v) in record {
                        keyset.insert(k);
                    }
                }
            }
        }
        for key in keyset{
            keyvec.push(key.clone());
        }
        keyvec
    }

    /// Create a new batch
    pub fn new(records: Vec<HashMap<String, Value>>) -> Self{
        Batch{
            records
        }
    }
}


#[cfg(test)]
pub mod tests{
    use serde_json::Value;
    use std::collections::HashMap;
    use crate::records::batch::Batch;

    #[test]
    fn should_create_batch(){
        let mut v = Vec::new();
        let mut r = HashMap::new();
        r.insert("test".to_string(), Value::String("test_string".to_string()));
        v.push(r);
        let b = Batch::new(v);
        assert_eq!(b.records.len(), 1);
    }

    #[test]
    fn should_convert_to_json(){
        let mut v = Vec::new();
        let mut r = HashMap::new();
        r.insert("test".to_string(), Value::String("test_string".to_string()));
        v.push(r);
        let b = Batch::new(v.clone());
        let b_str = b.to_json().unwrap();
        let j_str = serde_json::to_string(&v).unwrap();
        assert_eq!(b_str, j_str);
    }

    #[test]
    fn should_get_0_size_when_empty(){
        let v = Vec::new();
        let b = Batch::new(v);
        assert_eq!(b.size(), 0);
    }

    #[test]
    fn should_get_accurate_size(){
        let mut v = Vec::new();
        let mut r = HashMap::new();
        r.insert("test".to_string(), Value::String("test_string".to_string()));
        v.push(r.clone());
        v.push(r);
        let b = Batch::new(v.clone());
        assert_eq!(b.size(), 2);
    }

    #[test]
    fn should_drain_on_iterate(){
        let mut v = Vec::new();
        let mut r = HashMap::new();
        r.insert("test".to_string(), Value::String("test_string".to_string()));
        v.push(r.clone());
        v.push(r);
        let mut b = Batch::new(v.clone());
        assert_eq!(b.size(), 2);
        let _n = b.next().unwrap();
        assert_eq!(b.size(), 1);
    }

    #[test]
    fn should_be_able_to_reaccess_records_after_access(){
        let mut v = Vec::new();
        let mut r = HashMap::new();
        r.insert("test".to_string(), Value::String("test_string".to_string()));
        v.push(r.clone());
        v.push(r);
        let b = Batch::new(v.clone());
        let _v = b.get_records();
        assert_eq!(b.size(), 2);
    }

    #[test]
    fn should_get_keys_without_a_scan(){
        let mut v = Vec::new();
        let mut r = HashMap::new();
        r.insert("test".to_string(), Value::String("test_string".to_string()));
        v.push(r.clone());
        let mut r2 = HashMap::new();
        r2.insert("test".to_string(), Value::String("test_string".to_string()));
        r2.insert("test2".to_string(), Value::Bool(true));
        v.push(r2);
        let b = Batch::new(v.clone());
        let keys = b.get_keys(false);
        assert_eq!(keys.len(), 1)
    }

    #[test]
    fn should_get_keys_with_scan(){
        let mut v = Vec::new();
        let mut r = HashMap::new();
        r.insert("test".to_string(), Value::String("test_string".to_string()));
        v.push(r.clone());
        let mut r2 = HashMap::new();
        r2.insert("test".to_string(), Value::String("test_string".to_string()));
        r2.insert("test2".to_string(), Value::Bool(true));
        v.push(r2);
        let b = Batch::new(v.clone());
        let keys = b.get_keys(true);
        assert_eq!(keys.len(), 2);
    }
}