//! A basic record cache.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;

use serde_json::Value;
use crate::records::batch::Batch;

/// The record cache
#[derive(Clone, Debug)]
pub(in crate) struct RecordCache{
    pub records: Vec<HashMap<String, Value>>
}


impl RecordCache{

    /// Get the current size of the cache
    pub fn size(&self) -> usize{
        self.records.len()
    }

    /// Add records to the cache
    ///
    /// # Variables
    /// * `records` - The records to push
    pub fn add_records(&mut self, records: Vec<HashMap<String, Value>>) {
        for record in records{
            self.records.push(record);
        }
    }

    /// Add a record to the cache
    ///
    /// # Variable
    /// * `record` - The record to insert
    #[allow(dead_code)]
    pub fn add_record(&mut self, record: HashMap<String, Value>){
        self.records.push(record)
    }

    /// Set records in the cache.
    ///
    /// # Variable
    /// * `records` - Updated records
    pub fn update_records(&mut self, records: Vec<HashMap<String, Value>>){
        self.records = records
    }

    /// Reappend records to the vec
    ///
    /// # Variables - The batch to append
    pub fn append_to_front(&mut self, records: Batch){
        let mut append_records = records.get_records();
        if self.records.len() > 0{
            append_records.append(&mut self.records);
        }
        self.records = append_records;
    }

    /// Get a specific number of records
    ///
    /// # Variables
    /// * `n` - The number of records to pull
    pub fn get_n_records(&mut self, n: usize) -> Batch{
        let mut outrecords = vec![];
        let mut i = 0;
        while i < n && self.records.len() > 0{
            let r = self.records.pop().unwrap();
            outrecords.push(r);
            i += 1;
        }
        Batch::new(outrecords)
    }

    /// Get records from the store
    #[allow(dead_code)]
    pub fn get_records(&self) -> Vec<HashMap<String, Value>>{
        self.records.clone()
    }

    pub fn new() -> Self{
        RecordCache{
            records: vec![]
        }
    }
}


#[cfg(test)]
pub mod tests{
    use crate::records::cache::RecordCache;
    use std::collections::HashMap;
    use serde_json::{Value, Number};

    #[test]
    fn should_reappend_records(){
        let mut cache = RecordCache::new();
        let mut records = Vec::new();
        for _i in 0..100 {
            let mut record = HashMap::<String, Value>::new();
            record.insert("test".to_string(), Value::Number(Number::from(2)));
            records.push(record);
        }
        cache.add_records(records);
        assert_eq!(cache.size(), 100);
        let obatch = cache.get_n_records(93);
        assert_eq!(cache.size(), 7);
        cache.append_to_front(obatch);
        assert_eq!(cache.size(), 100);
    }

}