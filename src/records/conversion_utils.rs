//! Conversion utilities for queries
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;

use chrono::NaiveDateTime;
use regex::Regex;
use serde_json::Value;

/// Get the value as an object
///
/// # Variable
/// * `record`  -   The record to use
/// * `key` -   The hashmap key
pub fn get_value_as_object(
    record: &HashMap<String, Value>, key: &str) -> Option<HashMap<String, Value>>{
    let map_opt = record.get(key);
    if map_opt.is_some(){
        let map_val = map_opt.unwrap();
        let mpref = map_val.as_object();
        if mpref.is_some(){
            let mp = mpref.unwrap();
            let mut hmp = HashMap::new();
            for key in mp.keys(){
                let v = mp.get(key).unwrap();
                hmp.insert(key.clone(), v.clone());
            }
            Some(hmp)
        }else{
            None
        }
    }else{
        None
    }
}


/// Get the serde value in the map as a string
///
/// # Variable
/// * `record`  -   The record
/// * `key` -   The key of the value in the map
pub fn get_value_as_string(record: &HashMap<String, Value>, key: &str) -> Option<String>{
    let str_opt = record.get(key);
    if str_opt.is_some(){
        let str_val = str_opt.unwrap();
        let str = str_val.as_str();
        if str.is_some(){
            Some(str.unwrap().to_string())
        }else{
            None
        }
    }else{
        None
    }
}


/// Get the resulting value as a 64 bit integer
///
/// # Variable
/// * `record` - The record
/// * `key` - Key for the map
pub fn get_value_as_i64(record: &HashMap<String, Value>, key: &str) -> Option<i64>{
    let num_opt = record.get(key);
    if num_opt.is_some() {
        let num_val = num_opt.unwrap();
        num_val.as_i64()
    }else{
        None
    }
}


/// Get the resulting value as a 32 bit integer
///
/// # Variable
/// * `record` - The record
/// * `key` - Key for the map
pub fn get_value_as_i32(record: &HashMap<String, Value>, key: &str) -> Option<i32>{
    let num_opt = record.get(key);
    if num_opt.is_some() {
        let num_val = num_opt.unwrap();
        let n = num_val.as_i64().unwrap();
        Some(n as i32)
    }else{
        None
    }
}


/// Get the value as a float
///
/// # Variable
/// * `record` -    The record
/// * `key` -       Key for the value
pub fn get_value_as_f64(record: &HashMap<String, Value>, key: &str) -> Option<f64>{
    let num_opt = record.get(key);
    if num_opt.is_some() {
        let num_val = num_opt.unwrap();
        num_val.as_f64()
    }else{
        None
    }
}


/// Get the serialized chrono value as a datetime
///
///# Variable
///* `record`   -   The record
///* `key`      -   Key in the record
pub fn get_naive_value_as_naive_datetime(
    record: &HashMap<String, Value>, key: &str) -> Option<NaiveDateTime>{
    let chrono_opt = record.get(key);
    if chrono_opt.is_some() {
        let val = chrono_opt.unwrap();
        let chrono_str = val.as_str();
        if chrono_str.is_some(){
            let dt = NaiveDateTime::parse_from_str(
                chrono_str.unwrap(), "%Y-%m-%d %T");
            if dt.is_ok(){
                let timestamp = dt.unwrap();
                Some(timestamp)
            }else{
                None
            }
        }else{
            None
        }
    }else{
        None
    }
}

/// Takes in a value converted using to string and produces a cassandra datetime string.
pub fn get_naive_dt_value_as_cassandra_time_str(record: &HashMap<String, Value>, key: &str) -> Option<String>{
    let chrono_opt = record.get(key);
    if chrono_opt.is_some(){
        let val = chrono_opt.unwrap();
        let chrono_str = val.as_str();
        if chrono_str.is_some(){
            let re = Regex::new("\\..*").unwrap();
            let stamp = re.replace(chrono_str.unwrap(), "");
            Some(stamp.to_string())
        }else{
            None
        }
    }else{
        None
    }
}


#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;

    use chrono::NaiveDateTime;
    use serde_json::Value;

    use crate::records::conversion_utils::{get_naive_dt_value_as_cassandra_time_str, get_naive_value_as_naive_datetime};

    #[test]
    fn should_convert_naive_value_to_cassandra_time_str(){
        let now = NaiveDateTime::parse_from_str(
            "2020-11-12 00:46:44", "%Y-%m-%d %T").unwrap();
        let now_str = now.to_string();
        let mut hmap = HashMap::new();
        let v = Value::from(now_str);
        hmap.insert("timestamp".to_string(), v);
        let str = get_naive_dt_value_as_cassandra_time_str(
            &hmap, "timestamp");
        assert!(str.is_some());
        assert_eq!("2020-11-12 00:46:44", str.unwrap());
    }

    #[test]
    fn should_convert_naive_value_to_naive_datetime(){
        let now = NaiveDateTime::parse_from_str(
            "2020-11-12 00:46:44", "%Y-%m-%d %T").unwrap();
        let now_str = now.to_string();
        let mut hmap = HashMap::new();
        let v = Value::from(now_str);
        hmap.insert("timestamp".to_string(), v);
        let utc_opt = get_naive_value_as_naive_datetime(
            &hmap, "timestamp");
        assert!(utc_opt.is_some());
        let utc = utc_opt.unwrap();
        let utc_str = utc.to_string();
        assert_eq!("2020-11-12 00:46:44", utc_str);
    }
}
